/*
 * Integrate.h
 *
 *  Created on: May 28, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: 
 */

#ifndef SRC_INTEGRATE_H_
#define SRC_INTEGRATE_H_

#include <Image.h>
#include <Volume.h>

#include <Utility.h>

/*****************************************************************************************/
// integration kernel for a voxel with only sdf
__global__ void IntegrateKernel( Volume vol, const Image<float> depth, const Matrix4 invTrack,
    const Matrix4 K, const float mu, const float maxweight);

__global__ void IntegrateKernel( ColoredVolume vol, const Image<float> depth, const Image<uchar3> color, const Matrix4 invTrack,
    const Matrix4 K, const float mu, const float maxweight);

__global__ void IntegrateKernelIIR( Volume vol, const Image<float> depth, const Matrix4 invTrack,
    const Matrix4 K, const float mu, const float alpha);

/*****************************************************************************************/

#endif /* SRC_INTEGRATE_H_ */
