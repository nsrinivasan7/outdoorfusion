/*
 * FusionConfig.h
 *
 *  Created on: May 28, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: 
 */

#ifndef SRC_FUSIONCONFIG_H_
#define SRC_FUSIONCONFIG_H_

#include <Volume.h>
#include <vector>
#include <inttypes.h>

#include <opencv2/opencv.hpp>
#include <string>
#include <Common.h>

//Configuration File
//*********************************************************************
class FusionConfig {
public:
  uint3 volumeSize;           // size of the volume in voxels
  float3 volumeDimensions;    // real world dimensions spanned by the volume in meters

  bool combinedTrackAndReduce;// combine tracking and calculating linear system in one
  // this saves some time in tracking, but there is no per pixel output anymore

  float4 camera;              // camera configuration parameters
  uint2 framesize;            // size of the input depth images
  float nearPlane, farPlane;  // values for raycasting in meters
  float mu;                   // width of linear ramp, left and right of 0 in meters
  float maxweight;            // maximal weight for volume integration, controls speed of updates

  int radius;                 // bilateral filter radius
  float delta;                // gaussian delta
  float e_delta;              // euclidean delta

  float dist_threshold;       // 3D distance threshold for ICP correspondences
  float normal_threshold;     // dot product normal threshold for ICP correspondences
  std::vector<int> iterations;  // max number of iterations per level
  float track_threshold;      // percent of tracked pixels to accept tracking result

  dim3 imageBlock;            // block size for image operations
  dim3 raycastBlock;          // block size for Raycasting

  // default values
  FusionConfig()
  {
    volumeSize = make_uint3(512);
    volumeDimensions = make_float3(600.f);

    combinedTrackAndReduce = false;

    camera = make_float4(256,256,256,256);
    framesize = make_uint2(512,512);

    nearPlane = 0.4f;
    farPlane = 2000.0f;
    mu = 4.f; // unit * mu_grid
    maxweight = 100.0f;

    radius = 2;
    delta = 4.0f;
    e_delta = 0.1f;

    dist_threshold = 0.1f;
    normal_threshold = 0.8f;
    iterations.push_back( 5 );
    iterations.push_back( 5 );
    iterations.push_back( 5 );
    track_threshold = 0.15f;

    imageBlock = dim3(32,16);
    raycastBlock = dim3(32,8);
  }


  float stepSize() const {  return min(volumeDimensions)/max(volumeSize); }          // step size for raycasting

  void write(cv::FileStorage& fs) const
  {
        fs << "{" <<
        "VolumeWidth" << (int)(volumeSize.x) <<
        "VolumeHeight" << (int)(volumeSize.y) <<
        "VolumeDepth" << (int)(volumeSize.z) <<
        "VolumeWidthMeters" << (float)(volumeDimensions.x) <<
        "VolumeHeightMeters" << (float)(volumeDimensions.y) <<
        "VolumeDepthMeters" << (float)(volumeDimensions.z) <<
        "FocalLengthX" << (float)camera.x <<
        "FocalLengthY" << (float)camera.y <<
        "CameraCenterX" << (float)camera.z <<
        "CameraCenterY" << (float)camera.w <<
        "InputFrameWidth" << (int)framesize.x <<
        "InputFrameHeight" << (int)framesize.y <<
        "NearPlane" << (float)nearPlane <<
        "FarPlane" << (float)farPlane <<
        "Integrationmu" << (float)mu <<
        "Integrationweight" << (float)maxweight <<
        "BilateralFilterRadius" << (int)radius <<
        "BilateralFilterFixed" << (float)delta <<
        "BilateralFilterMovable" << (float)e_delta <<
        "ICPDistanceThreshold" << (float)dist_threshold <<
        "ICPNormalThreshold" << (float)normal_threshold <<
        "IterationsLevel0" << (int)iterations[0] <<
        "IterationsLevel1" << (int)iterations[1] <<
        "IterationsLevel2" << (int)iterations[2] <<
        "TrackingPixelPercentageRatio" << (float)track_threshold <<
        "CUDAImageBlockSizeWidth" << (int)imageBlock.x <<
        "CUDAImageBlockSizeHeight" << (int)imageBlock.y <<
        "CUDAImageBlockSizeDepth1" << (int)imageBlock.z <<
        "CUDARaycastBlockSizeWidth" << (int)raycastBlock.x <<
        "CUDARaycastBlockSizeHeight" << (int)raycastBlock.y <<
        "CUDARaycastBlockSizeDepth1" << (int)raycastBlock.z <<
        "}";
  }

//  void write(cv::FileStorage& fs, const std::string& s) const
//   {
//         fs << s << "{"
//         "VolumeWidth" << (int)(volumeSize.x) <<
//         "VolumeHeight" << (int)(volumeSize.y) <<
//         "VolumeDepth" << (int)(volumeSize.z) <<
//         "VolumeWidthMeters" << (float)(volumeDimensions.x) <<
//         "VolumeHeightMeters" << (float)(volumeDimensions.y) <<
//         "VolumeDepthMeters" << (float)(volumeDimensions.z) <<
//         "FocalLengthX" << (float)camera.x <<
//         "FocalLengthY" << (float)camera.y <<
//         "CameraCenterX" << (float)camera.z <<
//         "CameraCenterY" << (float)camera.w <<
//         "InputFrameWidth" << (int)framesize.x <<
//         "InputFrameHeight" << (int)framesize.x <<
//         "NearPlane" << (float)nearPlane <<
//         "FarPlane" << (float)farPlane <<
//         "Integrationmu" << (float)mu <<
//         "Integrationweight" << (float)maxweight <<
//         "BilateralFilterRadius" << (int)radius <<
//         "BilateralFilterFixed" << (float)delta <<
//         "BilateralFilterMovable" << (float)e_delta <<
//         "ICPDistanceThreshold" << (float)dist_threshold <<
//         "ICPNormalThreshold" << (float)normal_threshold <<
//         "IterationsLevel0" << (int)iterations[0] <<
//         "IterationsLevel1" << (int)iterations[1] <<
//         "IterationsLevel2" << (int)iterations[2] <<
//         "TrackingPixelPercentageRatio" << (float)track_threshold <<
//         "CUDAImageBlockSizeWidth" << (int)imageBlock.x <<
//         "CUDAImageBlockSizeHeight" << (int)imageBlock.y <<
//         "CUDAImageBlockSizeDepth1" << (int)imageBlock.z <<
//         "CUDARaycastBlockSizeWidth" << (int)raycastBlock.x <<
//         "CUDARaycastBlockSizeHeight" << (int)raycastBlock.y <<
//         "CUDARaycastBlockSizeDepth1" << (int)raycastBlock.z <<
//         "}";
//   }



  void read(const cv::FileNode& node)
  {
    volumeSize.x = (uint)(int)node["VolumeWidth"];
    volumeSize.y = (uint)(int)node["VolumeHeight"];
    volumeSize.z = (uint)(int)node["VolumeDepth"];
    volumeDimensions.x = (float)node["VolumeWidthMeters"];
    volumeDimensions.y = (float)node["VolumeHeightMeters"];
    volumeDimensions.z = (float)node["VolumeDepthMeters"];
    camera.x = (float)node["FocalLengthX"];
    camera.y = (float)node["FocalLengthY"];
    camera.z = (float)node["CameraCenterX"];
    camera.w = (float)node["CameraCenterY"];
    framesize.x = (uint)(int)node["InputFrameWidth"];
    framesize.y = (uint)(int)node["InputFrameHeight"];
    nearPlane = (float)node["NearPlane"];
    farPlane  = (float)node["FarPlane"];
    mu = (float)node["Integrationmu"];
    maxweight = (float)node["Integrationweight"];
    radius = (int)node["BilateralFilterRadius"];
    delta = (float)node["BilateralFilterFixed"];
    e_delta = (float)node["BilateralFilterMovable"];
    dist_threshold = (float)node["ICPDistanceThreshold"];
    normal_threshold = (float)node["ICPNormalThreshold"];
    iterations[0] = (float)node["IterationsLevel0"];
    iterations[1] =(float) node["IterationsLevel1"];
    iterations[2] = (float)node["IterationsLevel2"];
    track_threshold = (float)node["TrackingPixelPercentageRatio"];
    imageBlock.x = (uint)(int)node["CUDAImageBlockSizeWidth"];
    imageBlock.y = (uint)(int)node["CUDAImageBlockSizeHeight"];
    imageBlock.z = (uint)(int)node["CUDAImageBlockSizeDepth1"];
    raycastBlock.x = (uint)(int)node["CUDARaycastBlockSizeWidth"];
    raycastBlock.y = (uint)(int)node["CUDARaycastBlockSizeWidth"];
    raycastBlock.z = (uint)(int)node["CUDARaycastBlockSizeDepth1"];
  }
  void display()
  {
    std::cout << " Volume = ( " << volumeSize.x << " x " << volumeSize.y << " x" << volumeSize.z << ") \n"
        << " Volume(M) = ( " << volumeDimensions.x << " x " << volumeDimensions.y << " x " <<  volumeDimensions.z << " ) \n"
        << " Camera Parameters = [ " << camera.x << ", " << camera.y << " , " << camera.z << " , " << camera.w << " ] \n"
        << " Input Frame Size = [ " << framesize.x << " x " << framesize.y <<  "] \n"
        << " Near Plane = " << nearPlane << "\n"
        << " Far Plane = " << farPlane << "\n"
        << " Integration(mu) = " << mu << "\n"
        << " Integration(weight) = " << maxweight << "\n"
        << " BilateralFilter(Radius) = " << radius << "\n"
        << " BilateralFilter(Fixed) = " << delta << "\n"
        << " BilateralFilter(Movable) = " << e_delta << "\n"
        << " ICP(Distance Threshold) = " << dist_threshold << "\n"
        << " ICP(Normal Threshold) = " << normal_threshold << "\n"
        << " Iterations(Level 0) = " << iterations[0] << "\n"
        << " Iterations(Level 1) = " << iterations[1] << "\n"
        << " Iterations(Level 2) = " << iterations[2] << "\n"
        << " Tracking(Pixel percentage ratio) = " << track_threshold << "\n"
        << " CUDA(Image Block Size) = [ " << imageBlock.x  << " x " << imageBlock.y << " x " << imageBlock.z << "] \n"
        << " CUDA(Raycast Block Size Size) = [ " << raycastBlock.x << " x " << raycastBlock.y << " x " << raycastBlock.z << " ]\n";
  }
};

static void read(const cv::FileNode& node, FusionConfig& x, const FusionConfig& default_value = FusionConfig()) {
  if(node.empty())
    x = default_value;
  else
    x.read(node);
}

static void write(cv::FileStorage& fs, const std::string&, const FusionConfig& x)
{
  x.write(fs);
}

#endif
