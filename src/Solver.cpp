/*
 * Solver.cpp
 *
 *  Created on: May 28, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: 
 */

#include <Solver.h>

Matrix4 operator*( const Matrix4 & A, const Matrix4 & B){
    Matrix4 R;

    const float * Af = &(A.data[0].x);
    const float * Bf = &(B.data[0].x);

    Eigen::Matrix<float,4,4> Ae = Eigen::Map<const Eigen::Matrix<float,4,4,Eigen::RowMajor> >(Af);
    Eigen::Matrix<float,4,4> Be = Eigen::Map<const Eigen::Matrix<float,4,4,Eigen::RowMajor> >(Bf);

    Eigen::Matrix<float,4,4> Ce = Eigen::Map<Eigen::Matrix<float,4,4,Eigen::RowMajor> >(&(R.data[0].x));
    Ce = Ae * Be;

    Eigen::Map<Eigen::Matrix<float,4,4,Eigen::RowMajor> > (&(R.data[0].x), 4, 4) = Ce;
    return R;
}

Matrix4 toMatrix4( const gtsam::Pose3 & p){
    Matrix4 R;
    gtsam::Matrix4 M = p.matrix();
    Eigen::Matrix4f Mf = M.cast<float>(); //cast to float

    Eigen::Map<Eigen::Matrix<float,4,4,Eigen::RowMajor> > (&(R.data[0].x), 4, 4) = Mf;
    return R;
}

gtsam::Pose3 togtsam(const Matrix4& A)
{
  const float * Af = &(A.data[0].x);
  Eigen::Matrix<float,4,4> Ae = Eigen::Map<const Eigen::Matrix<float,4,4,Eigen::RowMajor> >(Af);

  return gtsam::Pose3(Ae.cast<double>());

}

Matrix4 inverse( const Matrix4& A )
{
    const float * Af = &(A.data[0].x);
    Eigen::Matrix<float,4,4> Ae = Eigen::Map<const Eigen::Matrix<float,4,4,Eigen::RowMajor> >(Af);

    Matrix4 R;
    Eigen::Matrix4f IAe = Ae.inverse();

    Eigen::Map<Eigen::Matrix<float,4,4,Eigen::RowMajor> > (&(R.data[0].x), 4, 4) = IAe;
    return R;

}


std::ostream & operator<<( std::ostream & out, const Matrix4 & m ){
    for(unsigned i = 0; i < 4; ++i)
        out << m.data[i].x << "  " << m.data[i].y << "  " << m.data[i].z << "  " << m.data[i].w << "\n";
    return out;
}

Eigen::MatrixXf makeJTJ( const Eigen::Matrix<float,1,21>& v )
{

    Eigen::MatrixXf C;
    C.resize(6,6);
    C = Eigen::Matrix<float,6,6>::Zero();

    C.row(0)          = v.block(0,0,  1,6);
    C.block(1,1, 1,5) = v.block(0,6,  1,5);
    C.block(2,2, 1,4) = v.block(0,11, 1,4);
    C.block(3,3, 1,3) = v.block(0,15, 1,3);
    C.block(4,4, 1,2) = v.block(0,18, 1,2);
    C(5,5) = v(0,20);

    for(int r = 1; r < 6; ++r)
        for(int c = 0; c < r; ++c)
            C(r,c) = C(c,r);

    return C;
}

// solve using svd
Eigen::Matrix<float,6,1> solveSVD( const Eigen::Matrix<float,1,27>& vals ){

    const Eigen::Matrix<float,1,6> b = vals.block(0,0,  1,6);
    const Eigen::MatrixXf C = makeJTJ(vals.block(0,6,  1,21));

    //std::cout << "b = \n" << b << std::endl;
    //std::cout << "C = \n" << C << std::endl;


    Eigen::Matrix<float,6,1> deltax = C.jacobiSvd(Eigen::ComputeThinU  | Eigen::ComputeThinV).solve(b.transpose());
    return deltax;
}

bool solveAndUpdate::solve()
{
    // Get a 8x32 Matrix
    Eigen::Matrix<float,8,32> values = Eigen::Map<const Eigen::Matrix<float,8,32,Eigen::RowMajor> >(data);

    for(int j = 1; j < 8; ++j)
        values.row(0) += values.row(j);

    Eigen::Matrix<float,6,1> x = solveSVD( values.block(0,1, 1, 27)); // get the 6x1 state update

    //do an incremental pose
    gtsam::Vector v(6);
    v << (double)x(3,0), (double)x(4,0), (double)x(5,0),(double)x(0,0), (double)x(1,0), (double)x(2,0);
    gtsam::Pose3 delta(gtsam::Pose3::Expmap(v) );
    pose = toMatrix4( delta ) * pose;

    //std::cout << "toMatrix4( delta ) : \n" <<  toMatrix4( delta ) << std::endl;
    //writenormxtoFile(x.norm());
    if(x.norm() < 1e-5)
        return false;

    return true;
}

void writenormxtoFile(float x) {
    FILE* f;
    f = fopen("normx.txt", "a+");
    if(f == NULL) printf("Could not open file !! \n");
    fprintf(f, "%f\n", x);
    fclose(f);
}
