/*
 * RangeData.cpp
 *
 *  Created on: Feb 12, 2017
 *      Author: nsrinivasan7
 */

#include "RangeData.h"
#include <boost/range/iterator_range.hpp>
#include <iostream>
#include <fstream>

using namespace boost::filesystem;
using namespace std;

//#define INVERSEDEPTH

void RangeData::listEXRfiles(const string foldername) {
	path p(foldername);
	if(is_directory(p)) {
		cout << "Reading Depth data from " << p << endl;
		rangefilenames.reset(new vector<string>);
		for(auto& entry : boost::make_iterator_range(directory_iterator(p), {})) {
			if(path(entry).extension().string() == ".exr")
				rangefilenames->push_back(path(entry).string());
		}
	}
}

void RangeData::listTXTfiles(const string foldername) {
	path p(foldername);
	if(is_directory(p)) {
		cout << "Reading Depth data from " << p << endl;
		rangefilenames.reset(new vector<string>);
		for(auto& entry : boost::make_iterator_range(directory_iterator(p), {})) {
			if(path(entry).extension().string() == ".txt")
				rangefilenames->push_back(path(entry).string());
		}
	}
}


cv::Mat RangeData::getRangeData(size_t frameNumber) {
	// we need the width and height here ..
	// create an opencv Matrix
	cv::Mat depth(height,width,CV_16UC1);
	float m;
	string v;
	string line;
	// now fill it with the values
	cout << "Reading depth file " <<rangefilenames->at(frameNumber) << endl;
	ifstream ifs(rangefilenames->at(frameNumber).c_str());
	size_t row = -1;

	while(getline(ifs,line))
	{
		row++;
		size_t col = 0;
		std::stringstream linestream(line);
		std::string value;

		while(getline(linestream,v,','))
		{
			stringstream ss; ss << v;
			ss >> m;
#ifndef INVERSEDEPTH
			depth.at<uint16_t>(row,col) = (uint16_t)m;
#else
			depth.at<uint16_t>(row,col) = (uint16_t)(1.0/m);
#endif
			col++;
		}
	}
	ifs.close();
	newdepthframeAvailable = true;
	return depth;
}

bool RangeData::getRangeData(size_t frameNumber, uint16_t* output) {
	rangemutex.lock();
	cv::Mat depthImage = getRangeData(frameNumber);
	if(newdepthframeAvailable) {
		memcpy(output, depthImage.data, depthImage.rows * depthImage.cols * sizeof(uint16_t));
		depthImage.deallocate();
		newdepthframeAvailable = false;
		rangemutex.unlock();
		return true;
	}
	else {
		rangemutex.unlock();
		return false;
	}
}
