/*
 * Render.h
 *
 *  Created on: May 28, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: Rendering Functions
 */

#ifndef SRC_RENDER_H_
#define SRC_RENDER_H_

#include <Image.h>
#include <Volume.h>

#include <cuda_gl_interop.h> // includes cuda_gl_interop.h

//***************************************************************************//
// Rendering Kernels
//***************************************************************************//

// fused depth map
__global__ void RenderFusedKernel(Image<uint16_t> out, const Image<float3> vertex);

// render the tracking image (binary colors)
//__global__ void RenderTrackKernel( Image<uchar4> out, const Image<TrackData> data );

// render the input Depth Image
__global__ void RenderDepthKernel( Image<uchar3> out, const Image<float> depth,
    const float nearPlane, const float farPlane);

// render basic texture mapping
__global__ void RenderTextureKernel( Image<uchar4> out, const Image<float3> vertex,
    const Image<float3> normal, const Image<uchar3> texture, const Matrix4 texproj, const float3 light);

__global__ void RenderTextureKernel( Image<uchar4> out, const Image<float3> color);

// renders into a grayscale intensity map with lightsource
__global__ void RenderLightKernel( Image<uchar4> out, const Image<float3> vertex,
    const Image<float3> normal, const float3 light, const float3 ambient );

//render the normals using RGB
__global__ void RenderNormalsKernel( Image<uchar3> out, const Image<float3> in );

__global__ void RenderVertex2DepthKernel( Image<float> out, const Image<float3> in );


//***************************************************************************//
// Rendering functions
//***************************************************************************//

// fused depth map
void RenderFused(Image<uint16_t> out, const Image<float3> & vertex);

// render the tracking image (binary colors)
//void RenderTrack( Image<uchar4> out, const Image<TrackData> & data );

// scales the depth map from near-far to 0-1
void RenderDepth( Image<uchar3> out, const Image<float> & depth, const float nearPlane, const float farPlane );

//render the normals using RGB
void RenderNormals( Image<uchar3> out, const Image<float3> & normal);

// renders into a grayscale intensity map with lightsource
void RenderLight( Image<uchar4> out, const Image<float3> & vertex, const Image<float3> & normal,
    const float3 light, const float3 ambient);
void RenderVolumeLight( Image<uchar4> out, const Volume & volume, const Matrix4 view,
    const float nearPlane, const float farPlane, const float largestep, const float3 light, const float3 ambient );

// show the input
void RenderInput( Image<float3> pos3D, Image<float3> normal, Image<float> depth,
    const Volume volume, const Matrix4 view, const float nearPlane, const float farPlane, const float step, const float largestep);

// render basic texture mapping
void RenderTexture( Image<uchar4> out, const Image<float3> & vertex, const Image<float3> & normal,
    const Image<uchar3> & texture, const Matrix4 & texproj, const float3 light);

void RenderTexture( Image<uchar4> out, const Image<float3> & vertex);

template <typename T> struct gl;

template<> struct gl<unsigned char> {
    static const int format=GL_LUMINANCE;
    static const int type  =GL_UNSIGNED_BYTE;
};

template<> struct gl<uchar3> {
    static const int format=GL_RGB;
    static const int type  =GL_UNSIGNED_BYTE;
};

template<> struct gl<uchar4> {
    static const int format=GL_RGBA;
    static const int type  =GL_UNSIGNED_BYTE;
};

template<> struct gl<float> {
    static const int format=GL_LUMINANCE;
    static const int type  =GL_FLOAT;
};

template<> struct gl<float3> {
    static const int format=GL_RGB;
    static const int type  =GL_FLOAT;
};

template<> struct gl<uint16_t> {
    static const int format=GL_LUMINANCE;
    static const int type  =GL_UNSIGNED_SHORT;
};


template <typename T, typename A>
inline void glDrawPixels( const Image<T, A> & i ){
    ::glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    ::glPixelStorei(GL_UNPACK_ROW_LENGTH, i.size.x);
    ::glDrawPixels(i.size.x, i.size.y, gl<T>::format, gl<T>::type, i.data());
}

#endif /* SRC_RENDER_H_ */
