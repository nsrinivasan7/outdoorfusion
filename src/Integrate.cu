
#include <Integrate.h>
#include <Utility.h>

/*****************************************************************************************/
__global__ void IntegrateKernel( Volume vol, const Image<float> depth, const Matrix4 invTrack, 
    const Matrix4 K, const float mu, const float maxweight)
{
  // get the center of the voxel \boldsymbol{p}
  uint3 pix = make_uint3(thr2pos2());
  float3 pos = invTrack * vol.pos(pix);

  // project \boldsymbol{p} onto a camera
  float3 cameraX = K * pos;

  // rotate the vieweinf direction to be in the direction of the camera
  // direction of delta in the viewing direction
  // unit of the delta is the size of the voxel element
  const float3 delta = rotate(invTrack, make_float3(0,0, vol.dim.z / vol.size.z));

  // same delta in the camera plane
  const float3 cameraDelta = rotate(K, delta);

  // move along the viewing direction of the camera
  for(pix.z = 0; pix.z < vol.size.z; ++pix.z, pos += delta, cameraX += cameraDelta)
  {
    if(pos.z < 0.0001f) // some near plane constraint
      continue;

    //get the corresponding pixel
    const float2 pixel = make_float2(cameraX.x/cameraX.z + 0.5f, cameraX.y/cameraX.z + 0.5f);

    // check boundary conditions
    if(pixel.x < 0 || pixel.x > depth.size.x-1 || pixel.y < 0 || pixel.y > depth.size.y-1)
      continue;

    // see if there is a valid depth measurement at the point 
    const uint2 px = make_uint2(pixel.x, pixel.y);
    if(depth[px] == 0)
      continue;

    // \lambda = ||K^{-1}\hat{x}||_{2}
    // \eta = \lambda{-1} || 
    const float diff = (depth[px] - cameraX.z) * sqrt(1+sq(pos.x/pos.z) + sq(pos.y/pos.z));
    if(diff > -mu)
    {
      const float sdf = fminf(1.f, diff/mu);
      float2 data = vol[pix];
      data.x = clamp((data.y*data.x + sdf)/(data.y + 1), -1.f, 1.f);
      data.y = fminf(data.y+1, maxweight);
      vol.set(pix, data);
    }
  }
}

/*****************************************************************************************/
__global__ void IntegrateKernelIIR( Volume vol, const Image<float> depth, const Matrix4 invTrack,
    const Matrix4 K, const float mu, const float alpha)
{
  // get the center of the voxel \boldsymbol{p}
  uint3 pix = make_uint3(thr2pos2());
  float3 pos = invTrack * vol.pos(pix);

  // project \boldsymbol{p} onto a camera
  float3 cameraX = K * pos;

  // rotate the vieweinf direction to be in the direction of the camera
  // direction of delta in the viewing direction
  // unit of the delta is the size of the voxel element
  const float3 delta = rotate(invTrack, make_float3(0,0, vol.dim.z / vol.size.z));

  // same delta in the camera plane
  const float3 cameraDelta = rotate(K, delta);

  // move along the viewing direction of the camera
  for(pix.z = 0; pix.z < vol.size.z; ++pix.z, pos += delta, cameraX += cameraDelta)
  {
    if(pos.z < 0.0001f) // some near plane constraint
      continue;

    //get the corresponding pixel
    const float2 pixel = make_float2(cameraX.x/cameraX.z + 0.5f, cameraX.y/cameraX.z + 0.5f);

    // check boundary conditions
    if(pixel.x < 0 || pixel.x > depth.size.x-1 || pixel.y < 0 || pixel.y > depth.size.y-1)
      continue;

    // see if there is a valid depth measurement at the point
    const uint2 px = make_uint2(pixel.x, pixel.y);
    if(depth[px] == 0)
      continue;

    // \lambda = ||K^{-1}\hat{x}||_{2}
    // \eta = \lambda{-1} ||
    const float diff = (depth[px] - cameraX.z) * sqrt(1+sq(pos.x/pos.z) + sq(pos.y/pos.z));
    if(diff > -mu)
    {
      const float sdf = fminf(1.f, diff/mu);
      float2 data = vol[pix];
      data.x = data.x * alpha + (1.0f - alpha) * sdf;
      data.y = 1.0f;
      vol.set(pix, data);
    }
  }
}

/*****************************************************************************************/

__global__ void IntegrateKernel( ColoredVolume vol, const Image<float> depth, const Image<uchar3> color, const Matrix4 invTrack, 
    const Matrix4 K, const float mu, const float maxweight)
{
  // get the center of the voxel \boldsymbol{p}
  uint3 pix = make_uint3(thr2pos2());
  float3 pos = invTrack * vol.pos(pix);

  // project \boldsymbol{p} onto a camera
  float3 cameraX = K * pos;

  // rotate the vieweinf direction to be in the direction of the camera
  // direction of delta in the viewing direction
  // unit of the delta is the size of the voxel element
  const float3 delta = rotate(invTrack, make_float3(0,0, vol.dim.z / vol.size.z));

  // same delta in the camera plane
  const float3 cameraDelta = rotate(K, delta);

  // move along the viewing direction of the camera
  for(pix.z = 0; pix.z < vol.size.z; ++pix.z, pos += delta, cameraX += cameraDelta)
  {
    if(pos.z < 0.0001f) // some near plane constraint
      continue;

    //get the corresponding pixel
    const float2 pixel = make_float2(cameraX.x/cameraX.z + 0.5f, cameraX.y/cameraX.z + 0.5f);

    // check boundary conditions
    if(pixel.x < 0 || pixel.x > depth.size.x-1 || pixel.y < 0 || pixel.y > depth.size.y-1)
      continue;

    if(pixel.x < 0 || pixel.x > color.size.x-1 || pixel.y < 0 || pixel.y > color.size.y-1)
      continue;

    // see if there is a valid depth measurement at the point 
    const uint2 px = make_uint2(pixel.x, pixel.y);
    if(depth[px] == 0)
      continue;

    // \lambda = ||K^{-1}\hat{x}||_{2}
    // \eta = \lambda{-1} || 
    const float diff = (depth[px] - cameraX.z) * sqrt(1+sq(pos.x/pos.z) + sq(pos.y/pos.z));
    const uchar3 rgb = color[px];
    if(diff > -mu)
    {
      const float sdf = fminf(1.f, diff/mu);
      float2 data = vol[pix];
      data.x = clamp((data.y*data.x + sdf)/(data.y + 1), -1.f, 1.f);
      data.y = fminf(data.y+1, maxweight);

      //set sdf
      vol.set(pix, data);

      // get the current voxel color
      float3 voxelcolor  = vol.getcolor(pix);
      float colorweight = vol.getcolorweight(pix);
      float newcolorweight = colorweight + 1.f;
      float r = clamp((voxelcolor.x * colorweight + (rgb.x/255.f))/newcolorweight, 0.f, 1.f);
      float g = clamp((voxelcolor.y * colorweight + (rgb.y/255.f))/newcolorweight, 0.f, 1.f);
      float b = clamp((voxelcolor.z * colorweight + (rgb.z/255.f))/newcolorweight, 0.f, 1.f);

      /*vol.setcolor(pix, make_uchar3(r,g,b));*/
//      float r = (float)rgb.z/255.f;float g = (float)rgb.y/255.f; float b = (float)rgb.x/255.f;
      vol.setcolor(pix, make_float3(r,g,b));
      vol.setcolorweight(pix, fminf(newcolorweight, maxweight));
    }
  }
}

/*****************************************************************************************/
