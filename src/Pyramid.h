/*
 * Pyramid.h
 *
 *  Created on: Jun 11, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: 
 */

#ifndef SRC_PYRAMID_H_
#define SRC_PYRAMID_H_

#include <Image.h>

// sample one pyramid down
// robust half sampling
// function to perform half sampling
__global__ void halfSampleRobust( Image<float> out, const Image<float> in,
        const float e_d, const int r );

template<int HALFSAMPLE, typename typeIn>
__global__ void mm2meters( Image<float> depth, const Image<typeIn> in )
{
    const uint2 pixel = thr2pos2();
    if ( pixel.x >= depth.size.x || pixel.y >= depth.size.y )
        return;

    depth[pixel] = in[pixel * (HALFSAMPLE + 1)];
}

template<int HALFSAMPLE, typename typeIn>
__global__ void mm2metersC( Image<uchar3> color, const Image<typeIn> in )
{
    const uint2 pixel = thr2pos2();
    color[pixel] = in[pixel * (HALFSAMPLE + 1)];
}

#endif /* SRC_PYRAMID_H_ */
