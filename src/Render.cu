#include <Render.h>
#include <Raycast.h>

//***************************************************************************//
// Rendering Kernels
//***************************************************************************//


// fused depth map
__global__ void RenderFusedKernel(Image<uint16_t> out, const Image<float3> vertex)
{
    const uint2 pos = thr2pos2();
    float3 v = vertex[pos];
    float f = v.z;
    out[pos] = uint16_t (f * 1000);
}


/*__global__ void RenderTrackKernel( Image<uchar4> out, const Image<TrackData> data )
{
    const uint2 pos = thr2pos2();
    switch(data[pos].result)
    {
    case 1: out[pos] = make_uchar4(128, 128, 128,0);  // ok
        break;
    case -1: out[pos] = make_uchar4(0, 0, 0,0);       // no input
        break;
    case -2: out[pos] = make_uchar4(255,0,0,0);       // not in image
        break;
    case -3:  out[pos] = make_uchar4(0,255,0,0);      // no correspondence
        break;
    case -4: out[pos] = make_uchar4(0,0,255,0);       // to far away
        break;
    case -5: out[pos] = make_uchar4(255,255,0,0);     // wrong normal
        break;
    }
}*/


__global__ void RenderDepthKernel( Image<uchar3> out, const Image<float> depth,
    const float nearPlane, const float farPlane)
{
    const float d = (clamp(depth.el(), nearPlane, farPlane) - nearPlane) / (farPlane - nearPlane);
    out.el() = make_uchar3(d * 255, d * 255, d * 255);
}

__global__ void RenderTextureKernel( Image<uchar4> out, const Image<float3> vertex,
    const Image<float3> normal, const Image<uchar3> texture, const Matrix4 texproj, const float3 light)
{
    if(normal.el().x == -2.0f)
        out.el() = make_uchar4(0,0,0,255); // yellow
    else {
        const float3 proj = texproj * vertex.el();
        const float2 projPixel = make_float2( proj.x / proj.z + 0.5f, proj.y / proj.z + 0.5f);
        
        const float3 diff = normalize(light - vertex.el());
        const float dir = fmaxf(dot(normal.el(), diff), 0.f); // * 255;
        if(projPixel.x < 0 || projPixel.x > texture.size.x-1 || projPixel.y < 0 || projPixel.y > texture.size.y-1 ){
            out.el() = make_uchar4(dir*255,dir*255,dir*255,255); // red
        } else {
            const uchar3 texcol = texture[make_uint2(projPixel.x, projPixel.y)];
            out.el() = make_uchar4(texcol.x*dir, texcol.y*dir, texcol.z*dir, 255); // green
        }
    }

}

__global__ void RenderTextureKernel( Image<uchar4> out, const Image<float3> color)
{
    const float3 col = clamp(color.el(), 0.f, 1.f)*255;
    out.el() = make_uchar4(col.x,col.y,col.z,255);
}

__global__ void RenderLightKernel( Image<uchar4> out, const Image<float3> vertex,
    const Image<float3> normal, const float3 light, const float3 ambient )
{
    if(normal.el().x == -2.0f)
        out.el() = make_uchar4(0,0,0,255);
    else {
        const float3 diff = normalize(light - vertex.el());
        const float dir = fmaxf(dot(normal.el(), diff), 0.f);
        const float3 col = clamp(make_float3(dir) + ambient, 0.f, 1.f)*255;
        out.el() = make_uchar4(col.x, col.y, col.z, 255);
    }
}

__global__ void RenderNormalsKernel( Image<uchar3> out, const Image<float3> in )
{
    float3 n = in.el();
    if(n.x == -2)
        out.el() = make_uchar3(0,0,0);
    else {
        n = normalize(n);
        out.el() = make_uchar3(n.x*128 + 128, n.y*128+128, n.z*128+128);
    }
}

//***************************************************************************//
// Rendering functions
//***************************************************************************//

// fused depth map
void RenderFused(Image<uint16_t> out, const Image<float3> & vertex) {
  dim3 block(16,16);
  RenderFusedKernel<<<divup(out.size, block), block>>>(out, vertex);
  cudaDeviceSynchronize();
}


// render the tracking image (binary colors)
//void RenderTrack( Image<uchar4> out, const Image<TrackData> & data) {
//  dim3 block(32,16);
//  RenderTrackKernel<<<divup(out.size, block), block>>>( out, data );
//}

// scales the depth map from near-far to 0-1
void RenderDepth( Image<uchar3> out, const Image<float> & depth, const float nearPlane, 
    const float farPlane) {
  dim3 block(32,16);
  RenderDepthKernel<<<divup(depth.size, block), block>>>( out, depth, nearPlane, farPlane );
}

//render the normals using RGB
void RenderNormals( Image<uchar3> out, const Image<float3> &  normal)
{
  dim3 block(20,20);
  RenderNormalsKernel<<<divup(normal.size, block), block>>>( out, normal );
}

// renders into a grayscale intensity map with lightsource
void RenderLight( Image<uchar4> out, const Image<float3> & vertex, const Image<float3> & normal,
    const float3 light, const float3 ambient)
{
  dim3 block(32,16);
  RenderLightKernel<<<divup(normal.size, block), block>>>( out, vertex, normal, light, ambient );
}

void RenderVolumeLight( Image<uchar4> out, const Volume & volume, const Matrix4 view,
    const float nearPlane, const float farPlane, const float largestep, const float3 light, 
    const float3 ambient )
{
  dim3 block(16,16);
  RaycastLightKernel<<<divup(out.size, block), block>>>( out,  volume, view, nearPlane, farPlane, 
      volume.dim.x/volume.size.x, largestep, light, ambient );
}

// show the input
void RenderInput( Image<float3> pos3D, Image<float3> normal, Image<float> depth,
    const Volume volume, const Matrix4 view, const float nearPlane, const float farPlane, 
    const float step, const float largestep)
{
  dim3 block(16,16);
  RaycastInputKernel<<<divup(pos3D.size, block), block>>>(pos3D, normal, depth, volume, 
      view, nearPlane, farPlane, step, largestep);
}

// render basic texture mapping
void RenderTexture( Image<uchar4> out, const Image<float3> & vertex, const Image<float3> & normal,
    const Image<uchar3> & texture, const Matrix4 & texproj, const float3 light)
{
  dim3 block(32,16);
  RenderTextureKernel<<<divup(normal.size, block), block>>>( out, vertex, normal, texture, texproj, light);
}

void RenderTexture( Image<uchar4> out, const Image<float3> & color)
{
  dim3 block(32,16);
  RenderTextureKernel<<<divup(color.size, block), block>>>( out, color);
}

