/*
 * natesh01_testProgram.h
 *
 *  Created on: Feb 12, 2015
 *      @Author: Natesh Srinivasan
 *      @brief : Main file for running Kinect with PCL
 */

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <iostream>
#include "CameraPoses.h"
#include "RangeData.h"
#include "RawImage.h"
#include <boost/shared_ptr.hpp>
#include <Image.h>
#include <FusionConfig.h>

#include <opencv2/opencv.hpp>

using namespace boost::filesystem;

int main(int argc, char *argv[]) {

//  cv::ocl::setUseOpenCL(false);

  Image<uint16_t, HostDevice> depthimage;
  uint2 framesize = make_uint2(512,512);
  depthimage.alloc(framesize);
  cudaCheckLastError();

  boost::shared_ptr<RangeData> rangedata(new RangeData(argv[1],512,512) );
//  RangeData rangedata(argv[1],512,512);

  uint16_t *val = (uint16_t*)malloc(sizeof(uint16_t)*512*512);
  rangedata->getRangeData(0,depthimage.data());
  printf("here \n");

//  uint2 val = make_uint2(256,256);
//  cout << depthimage[val] << endl;

  cv::Mat depthImage(512,512,CV_16UC1,depthimage.data());
  cudaCheckLastError();
  double min;
  double max;
  cv::minMaxIdx(depthImage, &min, &max);
  cout << "Min = " << min <<" \t Max = " << max << endl;
  cv::Mat adjMap;
//  cv::convertScaleAbs(depthImage, adjMap, 255 / max);

//  cv::imshow("DepthImage", adjMap);
//  cv::waitKey(0);
  cudaCheckLastError();

//  CameraPoses p(argv[1]);
//  gtsam::Pose3 val = p.getCameraPose(0);
//  cout << val << endl;
}
