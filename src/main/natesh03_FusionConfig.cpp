/*
 * natesh03_FusionConfig.cpp
 *
 *  Created on: Feb 14, 2017
 *      @Author: Natesh Srinivasan
 *      @brief: 
 */

// the images used in kinect fusion
#include <FusionConfig.h>

#define WRITER 1

#ifdef WRITER
int main(int argc, char **argv)
{
  FusionConfig m;

  if (argc != 2) {
    printf("Usage : ./natesh08_fusion params.xml/txt");
  }

  std::string filename = argv[1];
  { //write
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);
    fs << "params" << m;
  }
  return 0;
}

#else
int main(int argc, char **argv)
{
  FusionConfig m;

  if (argc != 2) {
    printf("Usage : ./natesh08_fusion params.xml/txt");
  }

  std::string filename = argv[1];

  { //read
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    fs["params"] >> m;
  }

  m.display();
  return 0;
}
#endif
