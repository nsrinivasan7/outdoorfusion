/*
 * natesh06_OpenGL.cpp
 *
 *  Created on: Feb 14, 2017
 *      @Author: Natesh Srinivasan
 *      @brief: sample code for opening mulitple opengl windows
 */

#include <GL/glut.h>

#include <opencv2/opencv.hpp>
#include <boost/thread/thread.hpp>

#define WIDTH 640
#define HEIGHT 480

using namespace std;

float angle = 0.0f;

int window1, window2;
void renderfunc1(void) {

  // Clear Color and Depth Buffers
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Reset transformations
  glLoadIdentity();
  // Set the camera
  gluLookAt(  0.0f, 0.0f, 10.0f,
              0.0f, 0.0f,  0.0f,
              0.0f, 1.0f,  0.0f);

  glRotatef(angle, 0.0f, 1.0f, 0.0f);

  glBegin(GL_TRIANGLES);
      glVertex3f(-2.0f,-2.0f, 0.0f);
      glVertex3f( 2.0f, 0.0f, 0.0);
      glVertex3f( 0.0f, 2.0f, 0.0);
  glEnd();

  angle+=0.1f;
  boost::this_thread::sleep (boost::posix_time::microseconds (1000));
  glutSwapBuffers();
}


void renderfunc2(void) {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBegin(GL_TRIANGLES);
        glVertex3f(-2,-2,-5.0);
        glVertex3f(2,0.0,-5.0);
        glVertex3f(0.0,2,-5.0);
    glEnd();

    glutSwapBuffers();
}

void idlefunc()
{
  if(window1)
  {
    glutSetWindow(window1);
    glutPostRedisplay();
  }

  if(window2)
  {
    glutSetWindow(window2);
    glutPostRedisplay();
  }


}

void  changeSize(int w, int h) {

    // Prevent a divide by zero, when window is too short
    // (you cant make a window of zero width).
    if(h == 0)
        h = 1;
    float ratio = 1.0* w / h;

    // Use the Projection Matrix
    glMatrixMode(GL_PROJECTION);

        // Reset Matrix
    glLoadIdentity();

    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);

    // Set the correct perspective.
    gluPerspective(45,ratio,1,1000);

    // Get Back to the Modelview
    glMatrixMode(GL_MODELVIEW);
}


int main(int argc, char **argv) {

  glutInit(&argc,argv);
  glutInitWindowPosition(1,1);
  glutInitWindowSize(WIDTH,HEIGHT);
  glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH);

  // create the first window
  window1 = glutCreateWindow("natesh07_GLUT1");
  glutDisplayFunc(renderfunc1);
  glutReshapeFunc(changeSize);
  glutIdleFunc(idlefunc);

  // create the second window
  glutInitWindowPosition(100,100);
  window2 = glutCreateWindow("natesh07_GLUT2");
  glutDisplayFunc(renderfunc2);
  glutReshapeFunc(changeSize);
  glutIdleFunc(idlefunc);

  glutMainLoop();

  return 1;
}
