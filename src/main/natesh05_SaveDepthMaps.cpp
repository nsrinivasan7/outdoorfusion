/*
 * natesh05_SaveDepthMaps.cpp
 *
 *  Created on: Feb 14, 2017
 *      @Author: Natesh Srinivasan
 *      @brief:
 */



#include <Image.h>
#include <Common.h>
#include <Render.h>
#include <Fusion.h>
#include <Render.h>
#include <Solver.h>
#include <RangeData.h>
#include <CameraPoses.h>
#include <RawImage.h>

#include <Eigen/Core>
#include <gtsam/geometry/Pose3.h>

#include <GL/glut.h>

#include <boost/shared_ptr.hpp>
#include <boost/timer.hpp>
#include <boost/thread.hpp>

#define DISPLAYWIDTH 512
#define DISPLAYHEIGHT 512

Image<uchar4, HostDevice> lightScene, lightModel, textureModel;
Image<uint16_t, HostDevice> depthF1;

Image<uint16_t, HostDevice> depthImage;
Image<uchar3, HostDevice> rgbImage,depthScene;
Image<float3, Device> pos, normals;
Image<float, Device> dep;

// the direction of light
const float3 light = make_float3(1, 1, -1.0);
const float3 ambient = make_float3(0.1, 0.1, 0.1);

// the global configuration file
FusionConfig config;
Fusion fusion;
gtsam::Pose3 initialPose;

//set the Range data and Camera Poses here

//boost::shared_ptr<RangeData> rangedata(new RangeData("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/ground_truth_txt",WIDTH,HEIGHT));
//boost::shared_ptr<CameraPoses> cameraPoses(new CameraPoses("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/prior"));
//boost::shared_ptr<RawImage> rawImage(new RawImage("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/raw"));

RangeData rangedata("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/ground_truth_txt",WIDTH,HEIGHT);
CameraPoses cameraPoses("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/prior");
RawImage rawImage("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/raw");
string resultfolder("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/result/fusedDepth%03d.txt");

bool reset = true;
int counter = 0;

using namespace std;

void allocate()
{
	rgbImage.alloc(config.framesize);
	depthImage.alloc(config.framesize);
	lightScene.alloc(config.framesize);
	lightModel.alloc(config.framesize);
	depthF1.alloc(config.framesize);
	pos.alloc(config.framesize);
	normals.alloc(config.framesize);
	dep.alloc(config.framesize);
	textureModel.alloc(config.framesize);
	depthScene.alloc(config.framesize);
}
bool writedepth(const string filename) {

	ofstream ofs(filename.c_str());
	//get the vertex

	RenderFused( depthF1.getDeviceImage(), fusion.vertexF1);

	//save the depth image
	for(int row = 0; row < HEIGHT;  row++) {
		for(int col = 0; col < WIDTH; col++) {
			ofs << depthF1[make_uint2(col,row)] << "\t";
		}
		ofs << "\n";
	}
	ofs.close();
}

// the main loop of kinect fusion
void *kinectfusion(void* input)
{
	/********************************************************/
	// Initialization
	fusion.Init(config);
	// set the initial pose
	double size = (double)(config.volumeDimensions.x);
	  initialPose = gtsam::Pose3::Expmap((gtsam::Vector(6) << 0.0, 0.0, 0.0, (double)(size/2),
	      (double)(size/2), 0.0).finished());
	  fusion.setPose(toMatrix4(initialPose));
//	initialPose = cameraPoses.getCameraPose(0);
	/********************************************************/

	//start with the second frame, the first is zero
	size_t frameNumber = 0;
	gtsam::Pose3 previousPose(initialPose);

	int cnt = 0;

	while(1)
	{
		while(!rangedata.getRangeData(frameNumber, depthImage.data()));
		rawImage.getRawImage(frameNumber,rgbImage.data());

		gtsam::Pose3 deltapose = cameraPoses.getCameraPose(frameNumber++);
		gtsam::Pose3 currentPose = previousPose.compose(deltapose);
		cout << currentPose << endl;

		fusion.setKinectDeviceDepth(depthImage.getDeviceImage());
		// track the camera against the current model

		fusion.updateCameraPose(toMatrix4(currentPose));
		// integrate and raycast
		fusion.Integrate();
		fusion.Raycast();

		counter++;
		cudaDeviceSynchronize();
		cudaCheckLastError();

		//wait for some tiem here
		boost::this_thread::sleep (boost::posix_time::microseconds (100000));

		previousPose = currentPose;
		char filename[512];
		sprintf(filename,resultfolder.c_str(),cnt++);
		writedepth(string(filename));
	}
}


int main(int argc, char **argv) {

	// create a fusion structure and read the configuration file
	if(argc > 1)
	{
		cv::FileStorage fs(argv[1], cv::FileStorage::READ);
		fs["params"] >> config;
	}
	config.display();

	//  rangedata.reset(new RangeData("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/ground_truth_txt",WIDTH,HEIGHT));
	//  cameraPoses.reset(new CameraPoses("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/prior"));
	//  rawImage.reset(new RawImage("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/raw"));

	// allocate memory
	allocate();
	kinectfusion(NULL);
	printf("Success \n");

}
