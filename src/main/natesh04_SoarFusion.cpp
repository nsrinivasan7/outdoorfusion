/*
 * natesh04_SoarFusion.cpp
 *
 *  Created on: Feb 14, 2017
 *      @Author: Natesh Srinivasan
 *      @brief: 
 */



#include <Image.h>
#include <Common.h>
#include <Render.h>
#include <Fusion.h>
#include <Render.h>
#include <Solver.h>
#include <RangeData.h>
#include <CameraPoses.h>
#include <RawImage.h>

#include <Eigen/Core>
#include <gtsam/geometry/Pose3.h>

#include <GL/glut.h>

#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>

#define DISPLAYWIDTH 512
#define DISPLAYHEIGHT 512

Image<uchar4, HostDevice> lightScene, lightModel, textureModel;
Image<uint16_t, HostDevice> depthImage;
Image<uchar3, HostDevice> rgbImage,depthScene;
Image<float3, Device> pos, normals;
Image<float, Device> dep;

// the direction of light
const float3 light = make_float3(1, 1, -1.0);
const float3 ambient = make_float3(0.1, 0.1, 0.1);

// the global configuration file
FusionConfig config;
Fusion fusion;
gtsam::Pose3 initialPose;

//set the Range data and Camera Poses here

//boost::shared_ptr<RangeData> rangedata(new RangeData("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/ground_truth_txt",WIDTH,HEIGHT));
//boost::shared_ptr<CameraPoses> cameraPoses(new CameraPoses("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/prior"));
//boost::shared_ptr<RawImage> rawImage(new RawImage("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/raw"));

RangeData rangedata("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/ground_truth_txt",WIDTH,HEIGHT);
CameraPoses cameraPoses("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/prior");
RawImage rawImage("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/raw");

pthread_mutex_t fusionlock;
pthread_cond_t fusioncond;

bool reset = true;
int counter = 0;

using namespace std;

void allocate()
{
	rgbImage.alloc(config.framesize);
	depthImage.alloc(config.framesize);
	lightScene.alloc(config.framesize);
	lightModel.alloc(config.framesize);
	pos.alloc(config.framesize);
	normals.alloc(config.framesize);
	dep.alloc(config.framesize);
	textureModel.alloc(config.framesize);
	depthScene.alloc(config.framesize);
}

//define the windows
int colorwindow, depthwindow, inputvertexwindow, vertexwindow, texturewindow;

void displaycolor(void) {

	pthread_mutex_lock(&fusionlock);
	pthread_cond_wait(&fusioncond, &fusionlock);

	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// flip the image
	int width = (int)rgbImage.size.x;
	int height = (int)rgbImage.size.y;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, 0, height, 0.1, 1);
	glPixelZoom(1, -1);
	glRasterPos3f(0, height, -0.3);
	glDrawPixels(rgbImage);

	//  sprintf(rgbImageFile,"rgb/Image_%0.3d.png", rgbcnt);
	//  imwrite(rgbImageFile, cv::Mat(HEIGHT,WIDTH,CV_8UC3, rgbImage.data()));
	//  rgbcnt++;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glutSwapBuffers();
	pthread_mutex_unlock(&fusionlock);
}

void displaydepth(void) {

	// Clear Color and Depth Buffers
	pthread_mutex_lock(&fusionlock);
	pthread_cond_wait(&fusioncond, &fusionlock);
	int width = (int)depthImage.size.x;
	int height = (int)depthImage.size.y;


	cv::Mat rawdepth(height,width,CV_16UC1, depthImage.data());
	cv::Mat hdrdepth;
	double cvmin,cvmax;
	cv::minMaxLoc(rawdepth, &cvmin, &cvmax);
	rawdepth.convertTo(hdrdepth, CV_8UC1, 255.0/(cvmax-cvmin));

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, 0, height, 0.1, 1);
	glPixelZoom(1, -1);
	glRasterPos3f(0, height, -0.3);
	glDrawPixels(width,height,GL_LUMINANCE,GL_UNSIGNED_BYTE,hdrdepth.data);

	//  sprintf(depthImageFile,"depth/Image_%0.3d.png", depthcnt);
	//  imwrite(depthImageFile, cv::Mat(HEIGHT,WIDTH,CV_8UC1, hdrdepth.data));
	//  depthcnt++;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glutSwapBuffers();
	pthread_mutex_unlock(&fusionlock);
}

void displayinputvertex(void)
{
	pthread_mutex_lock(&fusionlock);
	pthread_cond_wait(&fusioncond, &fusionlock);
	RenderLight( lightScene.getDeviceImage(), fusion.inputVertex[0], fusion.inputNormal[0], light, ambient);
	int width = (int)lightScene.size.x;
	int height = (int)lightScene.size.y;


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, 0, height, 0.1, 1);
	glPixelZoom(1, -1);
	glRasterPos3f(0, height, -0.3);
	glDrawPixels(lightScene);

	//  sprintf(inputvertexImageFile,"inputvertex/Image_%0.3d.png", inputvertexcnt);
	//  imwrite(inputvertexImageFile, cv::Mat(HEIGHT,WIDTH,CV_8UC4, lightScene.data()));
	//  inputvertexcnt++;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glutSwapBuffers();

	pthread_mutex_unlock(&fusionlock);
}
// the idle function

void displayvertex(void)
{
	pthread_mutex_lock(&fusionlock);
	pthread_cond_wait(&fusioncond, &fusionlock);
	RenderLight( lightModel.getDeviceImage(), fusion.vertexF1, fusion.normalF1, light, ambient);
	int width = (int)lightModel.size.x;
	int height = (int)lightModel.size.y;


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, 0, height, 0.1, 1);
	glPixelZoom(1, -1);
	glRasterPos3f(0, height, -0.3);
	glDrawPixels(lightModel);

	//  sprintf(vertexImageFile,"vertex/Image_%0.3d.png", inputvertexcnt);
	//  imwrite(vertexImageFile, cv::Mat(HEIGHT,WIDTH,CV_8UC4, lightModel.data()));
	//  vertexcnt++;


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glutSwapBuffers();

	pthread_mutex_unlock(&fusionlock);
}

void displaytexture(void)
{

	pthread_mutex_lock(&fusionlock);
	pthread_cond_wait(&fusioncond, &fusionlock);
	RenderTexture( textureModel.getDeviceImage(), fusion.vertexF1, fusion.normalF1, rgbImage.getDeviceImage(),
			getCameraMatrix(fusion.configuration.camera) * inverse(fusion.initpose), light);
	int width = (int)textureModel.size.x;
	int height = (int)textureModel.size.y;


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, 0, height, 0.1, 1);
	glPixelZoom(1, -1);
	glRasterPos3f(0, height, -0.3);
	glDrawPixels(textureModel);

	//  sprintf(textureImageFile,"texture/Image_%0.3d.png", texturecnt);
	//  imwrite(textureImageFile, cv::Mat(HEIGHT,WIDTH,CV_8UC4, textureModel.data()));
	//  texturecnt++;


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glutSwapBuffers();

	pthread_mutex_unlock(&fusionlock);

}

void idlefunc()
{
	if(colorwindow)
	{
		glutSetWindow(colorwindow);
		glutPostRedisplay();
	}

	if(depthwindow)
	{
		glutSetWindow(depthwindow);
		glutPostRedisplay();
	}

	if(vertexwindow)
	{
		glutSetWindow(vertexwindow);
		glutPostRedisplay();
	}

	if(inputvertexwindow)
	{
		glutSetWindow(inputvertexwindow);
		glutPostRedisplay();
	}

	if(texturewindow)
	{
		glutSetWindow(texturewindow);
		glutPostRedisplay();
	}

}

// the exit function
void exitFunc(void){
	fusion.Clear();
	cudaDeviceReset();
}

void keyboarfunc(unsigned char key, int x, int y)
{
	switch(key){
	case 'c':
		fusion.Reset();
		fusion.setPose(toMatrix4(initialPose));
		reset = true;
		counter = 0;
		break;
	case 'q':
		exit(0);
		break;
	}
}

void  rehsapefunc(int w, int h) {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glColor3f(1.0f,1.0f,1.0f);
	glRasterPos2f(-1, 1);
	//offsets to make (0,0) the top left pixel (rather than off the display)
	glOrtho(-0.375, w-0.375, h-0.375, -0.375, -1 , 1);
	glPixelZoom(1,-1);
}

// the main loop of kinect fusion
void *kinectfusion(void* input)
{
	/********************************************************/
	// Initialization
	fusion.Init(config);
	// set the initial pose
	double size = (double)(config.volumeDimensions.x);
	  initialPose = gtsam::Pose3::Expmap((gtsam::Vector(6) << 0.0, 0.0, 0.0, (double)(size/2),
	      (double)(size/2), 0.0).finished());
	  fusion.setPose(toMatrix4(initialPose));
//	initialPose = cameraPoses.getCameraPose(0);
	/********************************************************/

	//start with the second frame, the first is zero
	size_t frameNumber = 0;
	gtsam::Pose3 previousPose(initialPose);

	while(1)
	{
		pthread_mutex_lock(&fusionlock);
		while(!rangedata.getRangeData(frameNumber, depthImage.data()));
		rawImage.getRawImage(frameNumber,rgbImage.data());

		gtsam::Pose3 deltapose = cameraPoses.getCameraPose(frameNumber++);
		gtsam::Pose3 currentPose = previousPose.compose(deltapose);
		cout << currentPose << endl;

		fusion.setKinectDeviceDepth(depthImage.getDeviceImage());
		// track the camera against the current model

		fusion.updateCameraPose(toMatrix4(currentPose));
		// integrate and raycast
		fusion.Integrate();
		fusion.Raycast();

		counter++;
		cudaDeviceSynchronize();
		cudaCheckLastError();
		pthread_cond_signal(&fusioncond);
		pthread_mutex_unlock(&fusionlock);
		previousPose = currentPose;
	}
}

int main(int argc, char **argv) {

	// create a fusion structure and read the configuration file
	if(argc > 1)
	{
		cv::FileStorage fs(argv[1], cv::FileStorage::READ);
		fs["params"] >> config;
	}
	config.display();

	//  rangedata.reset(new RangeData("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/ground_truth_txt",WIDTH,HEIGHT));
	//  cameraPoses.reset(new CameraPoses("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/prior"));
	//  rawImage.reset(new RawImage("/home/nsrinivasan7/Mechanical/Dropbox/landscape_high/frontleft/raw"));

	// allocate memory
	allocate();

	//create the main kinect fusion thread
	pthread_t kinectfusion_thread;
	if(pthread_create(&kinectfusion_thread, NULL, kinectfusion, NULL) ){
		fprintf(stderr, "Error creating thread\n");
		return 1;
	}


	glutInit(&argc,argv);
	glutInitWindowPosition(1,1);
	glutInitWindowSize(WIDTH,HEIGHT);
	glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH);
	glutKeyboardFunc(keyboarfunc);

	// create the color window
	colorwindow = glutCreateWindow("Color");
	glutDisplayFunc(displaycolor);
	glutReshapeFunc(rehsapefunc);
	glutIdleFunc(idlefunc);
	glutKeyboardFunc(keyboarfunc);

	// create the depth window
	glutInitWindowPosition(DISPLAYWIDTH,0);
	depthwindow = glutCreateWindow("Depth");
	glutDisplayFunc(displaydepth);
	glutReshapeFunc(rehsapefunc);
	glutIdleFunc(idlefunc);
	glutKeyboardFunc(keyboarfunc);

	// create the input vertex window
	glutInitWindowPosition(DISPLAYWIDTH*2,0);
	inputvertexwindow = glutCreateWindow("Input Vertex");
	glutDisplayFunc(displayinputvertex);
	glutReshapeFunc(rehsapefunc);
	glutIdleFunc(idlefunc);
	glutKeyboardFunc(keyboarfunc);

	// create the integrated vertex window
	glutInitWindowPosition(DISPLAYWIDTH*3,0);
	vertexwindow = glutCreateWindow("Vertex");
	glutDisplayFunc(displayvertex);
	glutReshapeFunc(rehsapefunc);
	glutIdleFunc(idlefunc);
	glutKeyboardFunc(keyboarfunc);

	// create the integrated texture window
	glutInitWindowPosition(DISPLAYWIDTH*4,DISPLAYHEIGHT);
	texturewindow = glutCreateWindow("Texture");
	glutDisplayFunc(displaytexture);
	glutReshapeFunc(rehsapefunc);
	glutIdleFunc(idlefunc);
	glutKeyboardFunc(keyboarfunc);

	// run display loop until stopped
	glutMainLoop();

	if(pthread_join(kinectfusion_thread, NULL)) {
		fprintf(stderr, "Error joining thread\n");
		return 2;
	}

	printf("Success \n");

}
