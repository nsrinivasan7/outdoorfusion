/**
 * @file    testFuntion.h
 * @brief	Program for generating Bell numbers
 * @author  Natesh Srinivasan
 * @note
 */


#include <iostream>
#include <math.h>
#include "gtsam/base/Matrix.h"

using namespace std;
using namespace gtsam;

//TODO : Does not work beyond m = 25, can you fix this ?
std::size_t bellNumber(std::size_t M);


