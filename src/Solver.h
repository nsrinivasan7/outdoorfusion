/*
 * Solver.h
 *
 *  Created on: May 28, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: 
 */

#ifndef SRC_SOLVER_H_
#define SRC_SOLVER_H_

#include <Common.h>
#include <gtsam/geometry/Pose3.h>

inline Matrix4 EigentoMatrix4(Eigen::Matrix4f& in)
{
    Matrix4 R;
    for(int i = 0 ; i < 4; i++) {
            R.data[i].x = in(i,0);
            R.data[i].y = in(i,1);
            R.data[i].z = in(i,2);
            R.data[i].w = in(i,3);
    }
    return R;
}

Matrix4 toMatrix4( const gtsam::Pose3 & p);

gtsam::Pose3 togtsam(const Matrix4& m);

Eigen::MatrixXf makeJTJ( const Eigen::Matrix<float,1,21>& v );

Eigen::Matrix<float,6,1> solveSVD( const Eigen::Matrix<float,1,27>& vals );

void writenormxtoFile(float x);


#endif /* SRC_SOLVER_H_ */
