/*
 * RawImage.h
 *
 *  Created on: Feb 16, 2017
 *      Author: nsrinivasan7
 */

#ifndef RAWIMAGE_H_
#define RAWIMAGE_H_


#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>
#include <Common.h>

using namespace std;

class RawImage {
public:

  RawImage();

  RawImage(const string foldername) {
    listPNGfiles(foldername);
  }

  cv::Mat getRawImage(size_t frameNumber);
  bool getRawImage(size_t frameNumber, uchar3* data);


private:
  boost::shared_ptr<vector<string> > rangefilenames;
  size_t width;
  size_t height;

  void listFiles(const string foldername);
  void listPNGfiles(const string foldername);

};


#endif /* RAWIMAGE_H_ */
