#include <Fusion.h>
#include <Integrate.h>
#include <Raycast.h>
#include <Vertex.h>
#include <Pyramid.h>

#include <vector>

//*****************************************************************************************
// The Kinect Fusion Functions
//*****************************************************************************************

void Fusion::Init( const FusionConfig& config )
{
    configuration = config;
    cudaSetDeviceFlags(cudaDeviceMapHost);
    integration.init(config.volumeSize, config.volumeDimensions);
    vertex.alloc(config.framesize);
    vertexF1.alloc(config.framesize);
    normal.alloc(config.framesize);
    normalF1.alloc(config.framesize);
    rawDepth.alloc(config.framesize);
	
    inputDepth.resize(config.iterations.size());
    inputVertex.resize(config.iterations.size());
    inputNormal.resize(config.iterations.size());

    for (int i = 0; i < config.iterations.size(); ++i)
    {
        inputDepth[i].alloc(config.framesize >> i);
        inputVertex[i].alloc(config.framesize >> i);
        inputNormal[i].alloc(config.framesize >> i);
    }
    output.alloc(make_uint2(32, 8)); // 8 rows and 32 cols
    threadstats = true;
    Reset();
}

// the reset function
void Fusion::Reset()
{
    dim3 block(32, 16);
    dim3 grid = divup(dim3(integration.size.x, integration.size.y), block);
    if (threadstats) Print(grid, block, "Initialize Volume Kernel");
    InitVolumeKernel<<<grid, block>>>(integration, make_float2(1.0f, 0.0f));
}

// the clear function
void Fusion::Clear()
{
    integration.release();
}

void Fusion::setKinectDeviceDepth( const Image<uint16_t> & in )
{
    if (configuration.framesize.x == in.size.x) mm2meters<0> <<<
            divup(rawDepth.size, configuration.imageBlock),
            configuration.imageBlock>>>(rawDepth, in);
    else if (configuration.framesize.x == in.size.x / 2) mm2meters<1> <<<
            divup(rawDepth.size, configuration.imageBlock),
            configuration.imageBlock>>>(rawDepth, in);
    else
        printf("Something is wrong in the input pipeline \n");
}

void Fusion::updateDepthImage( const Image<float> & in )
{
    mm2meters<0> <<<divup(rawDepth.size, configuration.imageBlock),
            configuration.imageBlock>>>(rawDepth, in);
}

void Fusion::Raycast()
{
    // raycast integration volume into the depth, vertex, normal buffers
    raycastPose = pose;
    dim3 grid = divup(configuration.framesize, configuration.raycastBlock);
    if (threadstats) Print(grid, configuration.raycastBlock,
            "Raycast Volume Kernel");

    threadstats = false;
    RaycastKernel<<<grid, configuration.raycastBlock>>>(vertex, normal,
            integration,
            raycastPose * getInverseCameraMatrix(configuration.camera),
            configuration.nearPlane, configuration.farPlane,
            configuration.stepSize(), 0.75f * configuration.mu);
            
            RaycastKernel<<<grid, configuration.raycastBlock>>>(vertexF1, normalF1,
            integration,
            initpose * getInverseCameraMatrix(configuration.camera),
            configuration.nearPlane, configuration.farPlane,
            configuration.stepSize(), 0.75f * configuration.mu);
}

void Fusion::Integrate()
{

    dim3 grid = divup(dim3(integration.size.x, integration.size.y),
            configuration.imageBlock);
    if (threadstats) Print(grid, configuration.imageBlock,
            "Integration Volume Kernel");

    IntegrateKernel<<<grid, configuration.imageBlock>>>(integration, rawDepth,
            inverse(pose), getCameraMatrix(configuration.camera),
            configuration.mu, configuration.maxweight);
}
