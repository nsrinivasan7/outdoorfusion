#ifndef SRC_ERRORS_H_
#define SRC_ERRORS_H_

#include <Utility.h>
#include <stdio.h>

#define cudaCheckLastError() {                                                      \
    cudaError_t error = cudaGetLastError();                                         \
    if(error != cudaSuccess) {                                                      \
      printf("Cuda failure error in file '%s' in line %i: '%s' \n",                 \
          __FILE__,__LINE__, cudaGetErrorString(error));                            \
          exit(EXIT_FAILURE);                                                       \
    }                                                                               \
}                                                                                   \

#endif
