/*
 * RangeData.h
 *
 *  Created on: Feb 12, 2017
 *      Author: nsrinivasan7
 */

#ifndef RANGEDATA_H_
#define RANGEDATA_H_

#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class RangeMutex {
public:
	RangeMutex() {
		pthread_mutex_init( &m_mutex, NULL );
	}
	void lock() {
		pthread_mutex_lock( &m_mutex );
	}
	void unlock() {
		pthread_mutex_unlock( &m_mutex );
	}

	class ScopedLock {
		Mutex & _mutex;
	public:
		ScopedLock(Mutex & mutex): _mutex(mutex)
		{
			_mutex.lock();
		}

		~ScopedLock()
		{
			_mutex.unlock();
		}
	};
private:
	pthread_mutex_t m_mutex;
};

class RangeData {
public:

	RangeData();

	~RangeData() {};

	RangeData(const string foldername) {
		listEXRfiles(foldername);
		newdepthframeAvailable = false;
	}

	// constructor with the width and height of the range data specified.
	// This needs to be called if the range data is in the txt format

	RangeData(const string foldername, size_t w, size_t h) {
		width = w;
		height = h;
		listTXTfiles(foldername);
		newdepthframeAvailable = false;
	}

	cv::Mat getRangeData(size_t frameNumber);
	bool getRangeData(size_t frameNumber, uint16_t* data);


private:
	boost::shared_ptr<vector<string> > rangefilenames;
	size_t width;
	size_t height;

	RangeMutex rangemutex;
	bool newdepthframeAvailable;

	void listFiles(const string foldername);
	void listTXTfiles(const string foldername);
	void listEXRfiles(const string foldername);

};


#endif /* RANGEDATA_H_ */
