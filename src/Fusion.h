/*
 * Fusion.h
 *
 *  Created on: May 28, 2015
 *      @Author: Natesh Srinivasan
 *      @brief:  Kinect fusion wrappers around kernels
 */

#ifndef SRC_FUSION_H_
#define SRC_FUSION_H_

#include <Image.h>
#include <Volume.h>
#include <FusionConfig.h>

struct Fusion
{
    Volume integration;
    Image<float3, Device> vertex, normal, vertexF1, normalF1;

    std::vector<Image<float3, Device> > inputVertex, inputNormal;
    std::vector<Image<float, Device> > inputDepth;

    Image<float, Device> rawDepth;
    Image<float, HostDevice> output;

    FusionConfig configuration;

    Matrix4 pose, raycastPose, initpose;

    // print the thread statistics
    bool threadstats;

    // allocates the volume and image data on the device
    void Init( const FusionConfig & config );

    // releases the allocated device memory
    void Clear();

    // sets the current pose of the camera
    void setPose( const Matrix4 & p )
    {
        pose = p;
        initpose = p;
    }

    // high level API to run a simple tracking - reconstruction loop
    void Reset(); // removes all reconstruction information

    void Print( dim3 grid, dim3 block, const std::string& str )
    {
        printf("-----------------------------------------------\n");
        printf("%s\n", str.c_str());
        printf("Grid = [%u, %u, %u] \t Block = [%u, %u, %u] \n", grid.x, grid.y,
                grid.z, block.x, block.y, block.x);
        printf("-----------------------------------------------\n");
    }

    template<typename A>
    void setDepth( const Image<float, A> & depth )
    { // passes in a metric depth buffer as float array
        rawDepth = depth;
    }

    // passes in depth image in mm in 16-bit unsigned integers residing on the device
    void setKinectDeviceDepth( const Image<uint16_t> & );

    // overloaded function that takes a float image representing mm.
    void updateDepthImage( const Image<float> & );

      // overloaded function that takes a float image representing mm.
    void updateCameraPose( const Matrix4 & p) {
      pose = p;
    }
    
    // Raycast the reference images to track against from the current pose
    void Raycast();

  
    // Integrates the current depth map using the current camera pose
    void Integrate();

};

#endif /* SRC_FUSION_H_ */
