/*
 * testVolume.cpp
 *
 *  Created on: May 28, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: Unit test for function defined inth eVolume Class
 */

#include <Volume.h>

#include <CppUnitLite/TestHarness.h>
#include <gtsam/geometry/Pose3.h>

#include <opencv2/opencv.hpp>

#include <Eigen/Core>


using namespace std;
using namespace gtsam;

// TODO : Unit test for checking the raycasting utility functions
TEST(Volume, raycast1)
{

}

// TODO : Unit test for the interpolation
TEST(Volume, interpolation)
{

}

// TODO : Unit test for the gradient
TEST(Volume, gradient)
{

}

/* ************************************************************************* */
int main() {
TestResult tr;
return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */
