/*
 * testMemory.cpp
 *
 *  Created on: May 27, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: check for memory leaks
 */

//#include <BilateralFilter.h>
#include <Common.h>
#include <error.h>
#include <Utility.h>
#include <Errors.h>>

#include <CppUnitLite/TestHarness.h>
#include <gtsam/geometry/Pose3.h>

#include <opencv2/opencv.hpp>

#include <Eigen/Core>

using namespace std;
using namespace gtsam;

extern void HostToHost(float* in, int width, int height, float* out);
extern void HostToDevice(float* in, int width, int height,  float* out);
extern void DeviceToHost(float* in, int width, int height,  float* out);
extern void DeviceToDevice(float* in, int width, int height,  float* out);
extern void HostDeviceToHost1(float* in, int width, int height,  float* out);
extern void HostDeviceToHost2(float* in, int width, int height,  float* out);

#define WIDTH 8
#define HEIGHT 8
#define TOL 1e-9
void createMatrix(Eigen::MatrixXf& mat)
{
  mat.resize(WIDTH,HEIGHT);
  mat <<           0.2500,    0.2500,    0.2500,    0.2500,    0.5000,    0.5000,    0.5000,    0.5000,
                   0.2500,    0.2500,    0.2500,    0.2500,    0.5000,    0.5000,    0.5000,    0.5000,
                   0.2500,    0.2500,    0.2500,    0.2500,    0.5000,    0.5000,    0.5000,    0.5000,
                   0.2500,    0.2500,    0.2500,    0.2500,    0.5000,    0.5000,    0.5000,    0.5000,
                   0.7500,    0.7500,    0.7500,    0.7500,    1.0000,    1.0000,    1.0000,    1.0000,
                   0.7500,    0.7500,    0.7500,    0.7500,    1.0000,    1.0000,    1.0000,    1.0000,
                   0.7500,    0.7500,    0.7500,    0.7500,    1.0000,    1.0000,    1.0000,    1.0000,
                   0.7500,    0.7500,    0.7500,    0.7500,    1.0000,    1.0000,    1.0000,    1.0000;
}

TEST(gpuFusion, HostToHost)
{
 Eigen::MatrixXf expected, actual;
 createMatrix(expected);
 actual.resize(WIDTH,HEIGHT);
 HostToHost(expected.data(), WIDTH, HEIGHT, actual.data());
 cudaCheckLastError();
 CHECK(actual.isApprox(expected, TOL));
}

TEST(gpuFusion, HostToDevice)
{
  Eigen::MatrixXf expected, actual;
  createMatrix(expected);
  actual.resize(WIDTH,HEIGHT);
  HostToDevice(expected.data(), WIDTH, HEIGHT, actual.data());
  cudaCheckLastError();
  CHECK(actual.isApprox(expected,TOL));
}

TEST(gpuFusion, DeviceToHost)
{
  Eigen::MatrixXf expected, actual;
  createMatrix(expected);
  actual.resize(WIDTH,HEIGHT);
  DeviceToHost(expected.data(), WIDTH, HEIGHT, actual.data());
  cudaCheckLastError();
  CHECK(actual.isApprox(expected,TOL));
}

TEST(gpuFusion, DeviceToDevice)
{
  Eigen::MatrixXf expected, actual;
  createMatrix(expected);
  actual.resize(WIDTH,HEIGHT);
  DeviceToDevice(expected.data(), WIDTH, HEIGHT, actual.data());
  cudaCheckLastError();
  CHECK(actual.isApprox(expected,TOL));
}

TEST(gpuFusion, HostDeviceToHost1)
{
  Eigen::MatrixXf expected, actual;
  createMatrix(expected);
  actual.resize(WIDTH,HEIGHT);
  HostDeviceToHost1(expected.data(), WIDTH, HEIGHT, actual.data());
  cudaCheckLastError();
  CHECK(actual.isApprox(expected,TOL));
}

TEST(gpuFusion, HostDeviceToHost2)
{
  Eigen::MatrixXf expected, actual;
  createMatrix(expected);
  actual.resize(WIDTH,HEIGHT);
  HostDeviceToHost2(expected.data(), WIDTH, HEIGHT, actual.data());
  cudaCheckLastError();
  CHECK(actual.isApprox(expected,TOL));
}

/* ************************************************************************* */
int main() {
  TestResult tr;
  return TestRegistry::runAllTests(tr);
}
/* ************************************************************************* */

