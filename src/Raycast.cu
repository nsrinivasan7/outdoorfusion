#include <Raycast.h>

/***********************************************************************************/
// get vertex, normal from volume given a view
__global__ void RaycastKernel( Image<float3> pos3D, Image<float3> normal, const Volume volume,
    const Matrix4 view, const float nearPlane, const float farPlane, const float step, const float largestep)
{
  const uint2 pos = thr2pos2();

  const float4 hit = RaycastInlineKernel( volume, pos, view, nearPlane, farPlane, step, largestep );
  if(hit.w > 0){
    pos3D[pos] = make_float3(hit);
    float3 surfNorm = volume.grad(make_float3(hit));
    if(length(surfNorm) == 0){
      normal[pos].x = INVALID;
    } else {
      normal[pos] = normalize(surfNorm);
    }
  } else {
    pos3D[pos] = make_float3(0);
    normal[pos] = make_float3(INVALID, 0, 0);
  }
}

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

__global__ void RaycastKernel( Image<float3> pos3D, Image<float3> normal, Image<float3> tex3D, const ColoredVolume volume,
    const Matrix4 view, const float nearPlane, const float farPlane, const float step, const float largestep)
{
  const uint2 pos = thr2pos2();
  
  const float4 hit = RaycastInlineKernel( volume, pos, view, nearPlane, farPlane, step, largestep );
  if(hit.w > 0) {
    float3 posfloat =  make_float3(hit);
    
    pos3D[pos] = posfloat;
    // using make_float3 becasue see rayast.h, inlinekernel, interpolate
    float3 surfNorm = volume.grad(posfloat);
    
    if(length(surfNorm) == 0){
      normal[pos].x = INVALID;
    } else {
      normal[pos] = normalize(surfNorm);
      tex3D[pos] = volume.interpcolor(make_float3(hit.x,hit.y,hit.z));
    }
  } else {
    pos3D[pos] = make_float3(0);
    normal[pos] = make_float3(INVALID, 0, 0);
    tex3D[pos] = make_float3(0.f,0.f,0.f);
  }
}


/***********************************************************************************/
// get vertex, normal and depth from volume given a view

__global__ void RaycastInputKernel( Image<float3> pos3D, Image<float3> normal, Image<float> depth,
    const Volume volume, const Matrix4 view, const float nearPlane, const float farPlane, const float step, const float largestep)
{
    const uint2 pos = thr2pos2();
    
    float4 hit = RaycastInlineKernel( volume, pos, view, nearPlane, farPlane, step, largestep);
    if(hit.w > 0){
        pos3D[pos] = make_float3(hit);
        depth[pos] = hit.w;
        float3 surfNorm = volume.grad(make_float3(hit));
        if(length(surfNorm) == 0){
            normal[pos].x = -2;
        } else {
            normal[pos] = normalize(surfNorm);
        }
    } else {
        pos3D[pos] = make_float3(0); 
        normal[pos] = make_float3(0);
        depth[pos] = 0;
    }
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

__global__ void RaycastInputKernel( Image<float3> pos3D, Image<float3> normal, Image<float> depth,
    const ColoredVolume volume, const Matrix4 view, const float nearPlane, const float farPlane, 
    const float step, const float largestep)
{
    const uint2 pos = thr2pos2();
    
    float4 hit = RaycastInlineKernel( volume, pos, view, nearPlane, farPlane, step, largestep);
    if(hit.w > 0){
        pos3D[pos] = make_float3(hit);
        depth[pos] = hit.w;
        float3 surfNorm = volume.grad(make_float3(hit));
        if(length(surfNorm) == 0){
            normal[pos].x = -2;
        } else {
            normal[pos] = normalize(surfNorm);
        }
    } else {
        pos3D[pos] = make_float3(0);
        normal[pos] = make_float3(0);
        depth[pos] = 0;
    }
}




/***********************************************************************************/
// get phong shaded image from a volume and a view
__global__ void RaycastLightKernel( Image<uchar4> render, const Volume volume, const Matrix4 view, 
    const float nearPlane, const float farPlane, const float step, const float largestep, const float3 light, 
    const float3 ambient)
{
    const uint2 pos = thr2pos2();
    
    float4 hit = RaycastInlineKernel( volume, pos, view, nearPlane, farPlane, step, largestep);
    if(hit.w > 0)
    {
        const float3 test = make_float3(hit);
        const float3 surfNorm = volume.grad(test);
        if(length(surfNorm) > 0)
        {
            const float3 diff = normalize(light - test);
            const float dir = fmaxf(dot(normalize(surfNorm), diff), 0.f);
            const float3 col = clamp(make_float3(dir) + ambient, 0.f, 1.f) * 255;
            render.el() = make_uchar4(col.x, col.y, col.z,0);
        } else {
            render.el() = make_uchar4(0,0,0,0);
        }
    } else {
        render.el() = make_uchar4(0,0,0,0);
    }
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

__global__ void RaycastLightKernel( Image<uchar4> render, const ColoredVolume volume, const Matrix4 view, 
    const float nearPlane, const float farPlane, const float step, const float largestep, const float3 light, 
    const float3 ambient)
{
    const uint2 pos = thr2pos2();
    
    float4 hit = RaycastInlineKernel( volume, pos, view, nearPlane, farPlane, step, largestep);
    if(hit.w > 0)
    {
        const float3 test = make_float3(hit);
        const float3 surfNorm = volume.grad(test);
        if(length(surfNorm) > 0)
        {
            const float3 diff = normalize(light - test);
            const float dir = fmaxf(dot(normalize(surfNorm), diff), 0.f);
            const float3 col = clamp(make_float3(dir) + ambient, 0.f, 1.f) * 255;
            render.el() = make_uchar4(col.x, col.y, col.z,0);
        } else {
            render.el() = make_uchar4(0,0,0,0);
        }
    } else {
        render.el() = make_uchar4(0,0,0,0);
    }
}

/***********************************************************************************/

