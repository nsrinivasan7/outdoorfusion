#include <Pyramid.h>

/***************************************************************************/
// filter and halfsample

__global__ void halfSampleRobust( Image<float> out, const Image<float> in, const float e_d, const int r){
  const uint2 pixel = thr2pos2();
  const uint2 centerPixel = 2 * pixel;

  if(pixel.x >= out.size.x || pixel.y >= out.size.y )
    return;

  float sum = 0.0f;
  float t = 0.0f;
  const float center = in[centerPixel];
  for(int i = -r + 1; i <= r; ++i){
    for(int j = -r + 1; j <= r; ++j){
      float current = in[make_uint2(clamp(make_int2(centerPixel.x + j, centerPixel.y + i), make_int2(0), make_int2(in.size.x - 1, in.size.y - 1)))]; // TODO simplify this!
      if(fabsf(current - center) < e_d){
        sum += 1.0f;
        t += current;
      }
    }
  }
  out[pixel] = t / sum;
}

/***************************************************************************/