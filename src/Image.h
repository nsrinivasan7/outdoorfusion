/*
 * Image.h
 *
 *  Created on: May 27, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: definiton of Image related operations on a GPU
 */

#ifndef SRC_IMAGE_H_
#define SRC_IMAGE_H_

#include <stddef.h>
#include <Utility.h>
#include <Errors.h>

// a reference to the data
struct Ref {
    Ref( void *d = NULL) : data(d) {}
    void *data;
};

struct Host {
    Host() : data(NULL) {}
    ~Host() {CHECK_CUDA_ERROR(cudaFreeHost( data ));}

    void alloc( uint size ) { CHECK_CUDA_ERROR(cudaHostAlloc( &data, size, cudaHostAllocDefault)); }
    void * data;
};

struct Device {
    Device() : data(NULL) {}
    ~Device() {cudaFree( data ); }

    void alloc( uint size ) { CHECK_CUDA_ERROR(cudaMalloc( &data, size ));}
    void * data;
};

struct HostDevice {
    HostDevice() : data(NULL) {}
    ~HostDevice() { CHECK_CUDA_ERROR(cudaFreeHost( data )); }

    void alloc( uint size ) { CHECK_CUDA_ERROR(cudaHostAlloc( &data, size,  cudaHostAllocMapped )); }
    void * getDevice() const {
        void * devicePtr;
        CHECK_CUDA_ERROR(cudaHostGetDevicePointer(&devicePtr, data, 0));
        return devicePtr;
    }
    void * data;
};

inline void image_copy( Host & to, const Ref & from, uint size ){
    cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}

inline void image_copy( Device & to, const Ref & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyDeviceToDevice);
}

inline void image_copy( HostDevice & to, const Ref & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}

inline void image_copy( Host & to, const Host & from, uint size ){
  cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}

inline void image_copy( Host & to, const Device & from, uint size ){
    cudaMemcpy(to.data, from.data, size, cudaMemcpyDeviceToHost);
}

inline void image_copy( Host & to, const HostDevice & from, uint size ){
    cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}


inline void image_copy( Device & to, const Host & from, uint size ){
    cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToDevice);
}

inline void image_copy( Device & to, const Device & from, uint size ){
    cudaMemcpy(to.data, from.data, size, cudaMemcpyDeviceToDevice);
}

inline void image_copy( Device & to, const HostDevice & from, uint size ){
    cudaMemcpy(to.data, from.getDevice(), size, cudaMemcpyDeviceToDevice);
}

inline void image_copy( HostDevice & to, const Host & from, uint size ){
    cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}

inline void image_copy( HostDevice & to, const Device & from, uint size ){
    cudaMemcpy(to.getDevice(), from.data, size, cudaMemcpyDeviceToDevice);
}

inline void image_copy( HostDevice & to, const HostDevice & from, uint size ){
    cudaMemcpy(to.data, from.data, size, cudaMemcpyHostToHost);
}

template <typename T, typename Allocator = Ref>
struct Image : public Allocator {
    typedef T PIXEL_TYPE;
    uint2 size;

    Image() : Allocator() { size = make_uint2(0);  }
    Image( const uint2 & s ) { alloc(s);}

    void alloc( const uint2 & s ){
        if(s.x == size.x && s.y == size.y)
            return;
        Allocator::alloc( s.x * s.y * sizeof(T) );
        size = s;
    }

    __device__ T & el(){
        return operator[](thr2pos2());
    }

    __device__ const T & el() const {
        return operator[](thr2pos2());
    }

    __device__ T & operator[](const uint2 & pos ){
        return static_cast<T *>(Allocator::data)[pos.x + size.x * pos.y];
    }

    __device__ const T & operator[](const uint2 & pos ) const {
        return static_cast<const T *>(Allocator::data)[pos.x + size.x * pos.y];
    }

    Image<T> getDeviceImage() {
        return Image<T>(size, Allocator::getDevice());
    }

    operator Image<T>() {
        return Image<T>(size, Allocator::data);
    }

    template <typename A1>
    Image<T, Allocator> & operator=( const Image<T, A1> & other ){
      if(size.x != other.size.x || size.y != other.size.y )
        exit(EXIT_FAILURE);

        image_copy(*this, other, size.x * size.y * sizeof(T));
        return *this;
    }

    template <typename A1>
    Image<T, Allocator> & copyTo( Image<T, A1> & other ){
      if(size.x != other.size.x || size.y != other.size.y )
        exit(EXIT_FAILURE);

        image_copy(other,*this, size.x * size.y * sizeof(T));
        return *this;
    }

    T * data() {
        return static_cast<T *>(Allocator::data);
    }

    const T * data() const {
        return static_cast<const T *>(Allocator::data);
    }
};

template <typename T>
struct Image<T, Ref> : public Ref {
    typedef T PIXEL_TYPE;
    uint2 size;

    Image() { size = make_uint2(0,0); }
    Image( const uint2 & s, void * d ) : Ref(d), size(s) {}

    __device__ T & el(){
        return operator[](thr2pos2());
    }

    __device__ const T & el() const {
        return operator[](thr2pos2());
    }

    __device__ T & operator[](const uint2 & pos ){
        return static_cast<T *>(Ref::data)[pos.x + size.x * pos.y];
    }

    __device__ const T & operator[](const uint2 & pos ) const {
        return static_cast<const T *>(Ref::data)[pos.x + size.x * pos.y];
    }

    template <typename A1>
    Image<T, Ref> & operator=( const Image<T, A1> & other ){
      if(size.x != other.size.x || size.y != other.size.y )
        exit(EXIT_FAILURE);

        image_copy(*this, other, size.x * size.y * sizeof(T));
        return *this;
    }

    template <typename A1>
    Image<T, Ref> & copyTo( Image<T, A1> & other ){
      if(size.x != other.size.x || size.y != other.size.y )
        exit(EXIT_FAILURE);

        image_copy(other,*this, size.x * size.y * sizeof(T));
        return *this;
    }

    T * data() {
        return static_cast<T *>(Ref::data);
    }

    const T * data() const {
        return static_cast<const T *>(Ref::data);
    }
};

//*************************************************************************//
//Unit test memory functions ****************//
//*************************************************************************//

extern void HostToHost(float* in, int width, int height, float* out);
extern void HostToDevice(float* in, int width, int height,  float* out);
extern void DeviceToHost(float* in, int width, int height,  float* out);
extern void DeviceToDevice(float* in, int width, int height,  float* out);
extern void HostDeviceToHost1(float* in, int width, int height,  float* out);
extern void HostDeviceToHost2(float* in, int width, int height,  float* out);

#endif /* SRC_IMAGE_H_ */
