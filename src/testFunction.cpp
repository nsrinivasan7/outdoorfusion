
/**
 * @file    testFunction.cpp
 * @brief	Code for generating Bell Number/ Example Test Program
 * @author  Natesh Srinivasan
 * @note
 */

#include <testFunction.h>
#include <Eigen/Core>

#include <vector>

using namespace std;
using namespace gtsam;

// TODO: Do a tic-toc on this and see the computation time required
std::size_t bellNumber(std::size_t M)
{
	if (M == 0)
		return 1;

	Eigen::MatrixXi A = Eigen::MatrixXi::Zero(M, M);
	A(0,0) = 1;
	for(std::size_t i = 1; i < M; i++) {
		A(i,0) = A(i-1,i-1);
		for(std::size_t j = 1; j <M; j++)
			A(i,j) = A(i,j-1) + A(i-1,j-1);
	}

	return A(M-1,M-1);

};
