/*
 * CameraPoses.cpp
 *
 *  Created on: Feb 12, 2017
 *      Author: nsrinivasan7
 */

#include "CameraPoses.h"
#include <boost/range/iterator_range.hpp>
#include <iostream>
#include <fstream>

using namespace boost::filesystem;
using namespace std;

void CameraPoses::listTXTfiles(const string foldername) {
  path p(foldername);
  if(is_directory(p)) {
      cout << "Reading Cameras Poses from " << p << endl;
      posefilenames.reset(new vector<string>);
      for(auto& entry : boost::make_iterator_range(directory_iterator(p), {})) {
          if(path(entry).extension().string() == ".txt")
            posefilenames->push_back(path(entry).string());
      }
  }
}

gtsam::Pose3 CameraPoses::getCameraPose(size_t frameNumber) {
  std::ifstream ifs(posefilenames->at(frameNumber));
  gtsam::Vector6 xi;
  ifs >> xi(0) >> xi(1) >> xi(2);
  ifs >> xi(4) >> xi(5) >> xi(3);
  xi(4) = -xi(4);
  xi(5) = -xi(5);
  xi(3) = 0;
  ifs.close();
  return gtsam::Pose3::Expmap(xi);
}
