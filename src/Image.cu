#include "Image.h"
#include "Errors.h"

extern void HostToHost(float* in,int width, int height, float* out)
{
  uint2 size = make_uint2(width, height);
  Image<float, Ref> reference(size,(void*)in);
  
  //create a device Image
  Image<float, Host> inputImage(size);
  Image<float, Host> inputImageHost(size);
  
  reference.copyTo(inputImageHost); // copy from reference to host
  inputImageHost.copyTo(inputImage); // copy from host to host
  cudaCheckLastError();
   
  uint linearsize = sizeof(float) * width * height;
  cudaMemcpy(out, inputImage.data(), linearsize, cudaMemcpyHostToHost);
}

extern void HostToDevice(float* in,int width, int height, float* out)
{
  uint2 size = make_uint2(width, height);
  Image<float> reference(size,(void*)in);
  
  //create a device Image
  Image<float, Device> inputImage(size);
  Image<float, Host> inputImageHost(size);

  reference.copyTo(inputImageHost); // copy from reference to host
  inputImageHost.copyTo(inputImage); // copy from host to device
  cudaCheckLastError();
  
  uint linearsize = sizeof(float) * width * height;
  cudaMemcpy(out, inputImage.data(), linearsize, cudaMemcpyDeviceToHost);
}

extern void DeviceToHost(float* in,int width, int height, float* out)
{
  uint2 size = make_uint2(width, height);
  Image<float> reference(size,(void*)in);
  
  //create a device Image
  Image<float, Device> inputImage(size);
  Image<float, Host> inputImageHost1(size);
  Image<float, Host> inputImageHost2(size);

  reference.copyTo(inputImageHost1); // copy from reference to host
  inputImageHost1.copyTo(inputImage); // copy from host to device
  inputImage.copyTo(inputImageHost2);
  cudaCheckLastError();
  
  uint linearsize = sizeof(float) * width * height;
  cudaMemcpy(out, inputImageHost2.data(), linearsize, cudaMemcpyHostToHost);
}

extern void DeviceToDevice(float* in,int width, int height, float* out)
{
  uint2 size = make_uint2(width, height);
  Image<float> reference(size,(void*)in);
  
  //create a device Image
  Image<float, Device> inputImage1(size);
  Image<float, Device> inputImage2(size);
  
  Image<float, Host> inputImageHost(size);

  reference.copyTo(inputImageHost); // copy from reference to host
  inputImageHost.copyTo(inputImage1);
  inputImage1.copyTo(inputImage2); // copy from host to device
  cudaCheckLastError();
  
  uint linearsize = sizeof(float) * width * height;
  cudaMemcpy(out, inputImage2.data(), linearsize, cudaMemcpyDeviceToHost);
}

extern void HostDeviceToHost1(float* in,int width, int height, float* out)
{
  uint2 size = make_uint2(width, height);
  Image<float> reference(size,(void*)in);
  
  //create a device Image
  Image<float, HostDevice> inputImage(size);
  Image<float, Host> inputImageHost(size);

  reference.copyTo(inputImageHost); // copy from reference to host
  inputImageHost.copyTo(inputImage); // copy from host to Host
  cudaCheckLastError();
  
  uint linearsize = sizeof(float) * width * height;
  cudaMemcpy(out, inputImage.data(), linearsize, cudaMemcpyHostToHost);
  
}

extern void HostDeviceToHost2(float* in,int width, int height, float* out)
{
  uint2 size = make_uint2(width, height);
  Image<float> reference(size,(void*)in);
  
  //create a device Image
  Image<float, HostDevice> inputImage(size);
  Image<float, Host> inputImageHost(size);

  reference.copyTo(inputImageHost); // copy from reference to host
  inputImageHost.copyTo(inputImage); // copy from host to Host
  cudaCheckLastError();
  
  uint linearsize = sizeof(float) * width * height;
  cudaMemcpy(out, inputImage.data(), linearsize, cudaMemcpyDeviceToHost);
  
}
