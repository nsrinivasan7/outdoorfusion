/*
 * Utility.h
 *
 *  Created on: May 27, 2015
 *      @Author: Natesh Srinivasan
 *      @brief:  Common Functions between CPU and GPU
 */

#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <stdint.h>
#include <vector_types.h>
#include <vector_functions.h>

#define WIDTH 512
#define HEIGHT 512

struct Matrix4
{
  Matrix4() {}
  Matrix4( const float* ptr, bool transpose = false )
  {
      if ( !transpose )
      {
          for ( uint32_t i = 0; i < 4; i++ )
          {
              data[i] = make_float4( ptr[0], ptr[1], ptr[2], ptr[3]);
              ptr += 4;
          }
      }
      else
      {
          for ( uint32_t i = 0; i < 4; i++ )
          {
              data[i] = make_float4(ptr[0 + i], ptr[4 + i], ptr[8 + i], ptr[12 + i]);
          }
      }
  }

  float4 data[4];
};

inline bool checkEqual(Matrix4& A, Matrix4& B)
{
  for(int i = 0; i < 4; i ++) {
    if(A.data[i].x != B.data[i].x || A.data[i].y != B.data[i].y ||
        A.data[i].z != B.data[i].z || A.data[i].w != B.data[i].w)
      return false;
  }
  return true;
}

// these three functions are needed for the gpukinectfusion.cu to work
std::ostream & operator<<( std::ostream & out, const Matrix4 & m );

Matrix4 operator*( const Matrix4 & A, const Matrix4 & B);

Matrix4 inverse( const Matrix4 & A );

struct solveAndUpdate
{
  solveAndUpdate(): data(NULL){};
  solveAndUpdate(float* d, Matrix4& p): data(d), pose(p), width(32), height(8) {};
  float *data;
  Matrix4 pose;
  int width;
  int height;

  bool solve();

  float* dataPtr() { return data;};
  float operator()( const int a, const int b ) {
    return data[a * width + b];
  }

};

#endif
