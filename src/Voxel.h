/*
 * Voxel.h
 *
 *  Created on: Jun 11, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: 
 */

#ifndef SRC_VOXEL_H_
#define SRC_VOXEL_H_


// A voxel structure which has a sdf
// a color for each voxel

struct VoxelColor
{
  short2 sdf;

  // the color value
  float3 col;
  // the color weight
  float colorweight;

};

#endif /* SRC_VOXEL_H_ */
