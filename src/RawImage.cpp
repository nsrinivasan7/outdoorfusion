/*
 * RawImage.cpp
 *
 *  Created on: Feb 12, 2017
 *      Author: nsrinivasan7
 */

#include "RawImage.h"
#include <boost/range/iterator_range.hpp>
#include <iostream>
#include <fstream>

using namespace boost::filesystem;
using namespace std;

void RawImage::listPNGfiles(const string foldername) {
  path p(foldername);
  if(is_directory(p)) {
    cout << "Reading Image data from " << p << endl;
    rangefilenames.reset(new vector<string>);
    for(auto& entry : boost::make_iterator_range(directory_iterator(p), {})) {
      if(path(entry).extension().string() == ".png")
        rangefilenames->push_back(path(entry).string());
    }
  }
}


cv::Mat RawImage::getRawImage(size_t frameNumber) {

  // we need the width and height here ..
  // create an opencv Matrix
  cout << "Reading image file " <<rangefilenames->at(frameNumber) << endl;
  return(cv::imread(rangefilenames->at(frameNumber),cv::IMREAD_ANYCOLOR));
}

bool RawImage::getRawImage(size_t frameNumber, uchar3* output) {

    cv::Mat rgbMat = getRawImage(frameNumber);
    // unsafe because we do not know if output has assigned data
    memcpy(output, rgbMat.data, rgbMat.rows * rgbMat.cols * sizeof(uchar3));
    return true;
}
