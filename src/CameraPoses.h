/*
 * CameraPoses.h
 *
 *  Created on: Feb 12, 2017
 *      Author: nsrinivasan7
 */

#ifndef CAMERAPOSES_H_
#define CAMERAPOSES_H_

#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>

#include <gtsam/geometry/Pose3.h>

using namespace std;

class CameraPoses {
public:
  CameraPoses();

  CameraPoses(const string foldername) {
    listTXTfiles(foldername);
  }

  gtsam::Pose3 getCameraPose(size_t frameNumber);

private:

  boost::shared_ptr<vector<string> > posefilenames;

  void listFiles(const string foldername);
  void listTXTfiles(const string foldername);
  void listXMLfiles(const string foldername);


};


#endif /* CAMERAPOSES_H_ */
