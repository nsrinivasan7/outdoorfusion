#include <Volume.h>
#include <Utility.h>

__global__ void InitVolumeKernel( Volume volume, const float2 val ){
    uint3 pos = make_uint3(thr2pos2());
    for(pos.z = 0; pos.z < volume.size.z; ++pos.z)
        volume.set(pos, val);
}


__global__ void InitVolumeKernel( ColoredVolume volume, const float2 val ){
    uint3 pos = make_uint3(thr2pos2());
    for(pos.z = 0; pos.z < volume.size.z; ++pos.z) {
        volume.set(pos, val);
        volume.setcolor(pos, make_float3(0.f,0.f,0.f));
        volume.setcolorweight(pos, 0.f);
    }
}

