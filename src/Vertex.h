/*
 * Vertex.h
 *
 *  Created on: May 28, 2015
 *      @Author: Natesh Srinivasan
 *      @brief: Converting between depth and vertex types
 */

#ifndef SRC_VERTEX_H_
#define SRC_VERTEX_H_

#include <Utility.h>
#include <Image.h>

__global__ void depth2vertex( Image<float3> vertex, const Image<float> depth, const Matrix4 invK );

__global__ void vertex2normal( Image<float3> normal, const Image<float3> vertex );

// functions for unit test
extern void depth2vertex();

extern void vertex2depth();

#endif /* SRC_VERTEX_H_ */
