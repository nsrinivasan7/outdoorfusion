function integrate(camRtC2W)
global ViewFrustumC;
ViewFrustumW = transformPoints(ViewFrustumC, camRtC2W);

range2test = [min(ViewFrustumW, [], 2) max(ViewFrustumW, [], 2)];

global XYZcam;
global voxel;
global K;
global tsdf_value;
global tsdf_weight;

global tsdf_variance;
global tsdf_average;
global frameNumber;
global max_variance;

global width
global height

max_tsdf_variance = max_variance;

% choose a bounding box to contain viewing frustum
rangeGrid = (range2test - voxel.range(:,[1 1])) / voxel.unit + 1;
rangeGrid(:,1) = max(1,floor(rangeGrid(:,1)));
rangeGrid(:,2) = min(ceil (rangeGrid(:,2)),voxel.size_grid);
rangeGrid = int32(rangeGrid);

% get the grid there
[X,Y,Z]=meshgrid(rangeGrid(1,1):rangeGrid(1,2),rangeGrid(2,1):rangeGrid(2,2),rangeGrid(3,1):rangeGrid(3,2)); % strange matlab syntax

X = X(:)'; Y = Y(:)'; Z = Z(:)';
gridIndex = sub2ind(voxel.size_grid',X,Y,Z);
gridCoordinateW = [single(X)*voxel.unit + voxel.range(1,1); single(Y)*voxel.unit + voxel.range(2,1); ...
    single(Z)*voxel.unit + voxel.range(3,1)];
clear X Y Z;

% transform the grid
gridCoordinateC = transformPoints(gridCoordinateW, camRtC2W, true);

% select: in front of camera
isValid = find(gridCoordinateC(3,:)>0);
gridCoordinateC = gridCoordinateC(:,isValid);
gridIndex = gridIndex(isValid);

% select: project 
px = round(K(1,1)*(gridCoordinateC(1,:)./gridCoordinateC(3,:)) + K(1,3));
py = round(K(2,2)*(gridCoordinateC(2,:)./gridCoordinateC(3,:)) + K(2,3));
isValid = (1<=px & px <= width & 1<=py & py<= height);
gridCoordinateC = gridCoordinateC(:,isValid);
gridIndex = gridIndex(isValid);
py = py(isValid);
px = px(isValid);

% select: valid depth
ind = sub2ind([height width],py,px);
isValid = XYZcam(ind+width*height*3)~=0;
gridCoordinateC = gridCoordinateC(:,isValid);
gridIndex = gridIndex(isValid);
ind = ind(isValid);

% compare distance between measurement and the grid
eta = (XYZcam(ind+width*height*2)- gridCoordinateC(3,:)) .* ((1+ (gridCoordinateC(1,:)./gridCoordinateC(3,:)).^2 + ...
    (gridCoordinateC(2,:)./gridCoordinateC(3,:)).^2 ).^0.5);

% select: > - mu
isValid = eta>-voxel.mu;
eta = eta(isValid);
gridIndex = gridIndex(isValid);
%ind = ind(isValid);
new_value = min(1,eta/voxel.mu);

% update the statistics
if(frameNumber == 1)
    tsdf_average(gridIndex) = tsdf_value(gridIndex);
else
    old_average  = tsdf_value(gridIndex);
    tsdf_average(gridIndex) = (1/frameNumber)* (new_value + (frameNumber-1)*old_average);
    old_variance = tsdf_variance(gridIndex);
    tsdf_variance(gridIndex) = ((frameNumber-2)/(frameNumber-1))*old_variance + ...
        (1/frameNumber)*(new_value - old_average).*(new_value - old_average);
    
    isValid = tsdf_variance(gridIndex) < max_tsdf_variance;   
    lefgridIndex = gridIndex(~isValid);
    gridIndex = gridIndex(isValid);
    eta = eta(isValid);
    new_value = min(1,eta/voxel.mu);
    
    if(frameNumber > 2)
        tsdf_value(lefgridIndex) = 0.0;
    end
        
end
    
old_weight = tsdf_weight(gridIndex); 
new_weight = old_weight + 1;   
tsdf_weight (gridIndex)= new_weight;
tsdf_value (gridIndex) = (tsdf_value(gridIndex).*old_weight +new_value)./new_weight;

frameNumber = frameNumber + 1;