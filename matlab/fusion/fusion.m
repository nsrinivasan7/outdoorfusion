function newDepth = fusion(startnum,endnum)

close all;
%uncomment this if you do not want to see the figures.
%set(0,'DefaultFigureVisible','off');

%% define the global parameters
global noise; %sigma of artificial noise
% global parameters
global VMap;
global NMap;
global XYZcam;
global Ncam;

global K; %calibration matrix

        
K = [751.218,  000.000, 582.47;
     000.000,  750.812, 585.00;
     000.000,  000.000, 001.00];

%% width and height of the image and range maps
global width;
global height;
width = 1200;
height = 1200;

%% the near and far field for raycasting and rendering
global nearF;
global farF;
nearF = 0.4;
farF = 2000;

%% define the voxel parameters
global voxel;
global tsdf_value;
global tsdf_weight;

unit = 4.0;
skip = 1;
voxel.unit = unit;
voxel.mu_grid = 4;
voxel.size_grid = [128; 256; 1024];
voxel.range(1,1) = -voxel.size_grid(1) * voxel.unit / 2.0;
voxel.range(1,2) = voxel.range(1,1) + (voxel.size_grid(1)-1) * voxel.unit;
voxel.range(2,1) = - voxel.size_grid(2) * voxel.unit / 2.0;
voxel.range(2,2) = voxel.range(2,1) + (voxel.size_grid(2)-1) * voxel.unit;
voxel.range(3,1) = -1.0; % - voxel.size_grid(3) * voxel.unit / 2;
voxel.range(3,2) = voxel.range(3,1) + (voxel.size_grid(3)-1) * voxel.unit;
voxel.mu = voxel.mu_grid * voxel.unit;
fprintf('memory = %f GB\n',  prod(voxel.size_grid) * 4 / (1024*1024*1024));
fprintf('space = %.2f m x %.2f m x %.2f m ', voxel.size_grid(1) * voxel.unit, voxel.size_grid(2) * voxel.unit, voxel.size_grid(3) * voxel.unit);
fprintf('= [%.2f,%.2f] x [%.2f,%.2f] x [%.2f,%.2f]\n',voxel.range(1,1),voxel.range(1,2),voxel.range(2,1),voxel.range(2,2),voxel.range(3,1),voxel.range(3,2));

%% define the stastical robust updation parameters

global tsdf_variance;
global tsdf_average;

global frameNumber; %for computing the running variance
frameNumber = 1;

global max_variance;
max_variance = 0.1; %1.0 is maximum

%% specify the dataset and assosiated folder where to save the results
masterfolder = '/home/nsrinivasan7/Dropbox/NIRS3_12-6-2016';
depthfileformat = strcat(masterfolder, '/cam/range_txt/range_txt_%08d.txt');
posefileformat = strcat(masterfolder, '/cam/prior/prior_%08d.txt');
priorfileformat = strcat(masterfolder, '/cam/prior/prior_%08d.txt');
imgfileformat = strcat(masterfolder,'/cam/raw/raw_%08d.png');
saveimageformat = strcat(masterfolder,'/%f/%0.1fx256x512x2048-skip%d/allframes/%d/result_%d.png');
dirformat = strcat(masterfolder,'/%f/%0.1fx256x512x2048-skip%d/allframes/%d');

%% specify the view frustrum for rendering

f = K(1,1);
global ViewFrustumC;
global raycastingDirectionC;

ViewFrustumC = [...
    0 -width/2 -width/2  width/2  width/2;
    0 -height/2  height/2  height/2 -height/2;
    0    f    f    f    f];
ViewFrustumC = ViewFrustumC/f * farF;

[pX,pY]=meshgrid(1:width,1:height);

raycastingDirectionC = [pX(:)'-K(1,3); pY(:)'-K(2,3); f*ones(1,width*height)]; % clipping at 8 meter is the furthest depth of kinect
raycastingDirectionC = raycastingDirectionC ./ repmat(sqrt(sum(raycastingDirectionC.^2,1)),3,1);

%% reset the weight and values of the tsdf
tsdf_value  =  ones([voxel.size_grid(1),voxel.size_grid(2), voxel.size_grid(3)],'single');
tsdf_weight = zeros([voxel.size_grid(1),voxel.size_grid(2), voxel.size_grid(3)],'single');
tsdf_variance  =  zeros([voxel.size_grid(1),voxel.size_grid(2), voxel.size_grid(3)],'single');
tsdf_average  =  zeros([voxel.size_grid(1),voxel.size_grid(2), voxel.size_grid(3)],'single');

dirname = sprintf(dirformat,max_variance,unit, skip,startnum);
if(exist(dirname,'dir'))
    rmdir(dirname);
    mkdir(dirname);
else
    mkdir(dirname);
end

%cameraRtC2W = readPosesWhalley(startnum,endnum,skip, posefileformat,priorfileformat);
usePrior = true;
[cameraRtC2W, camIndices] = readPosesNIRS3( priorfileformat, posefileformat, ...
    startnum, endnum, skip, usePrior);

%display related stuff
dispclim = [0 5];
k = 1;
h = figure('units','normalized','outerposition',[0 0 1 1]);
hold on;

% display variables
origdepth = [];
origimage =[];
RGB34 = [];
Map = jet(255);

for frameNr = startnum : skip : endnum
    
    fprintf('================================ Frame %d ================================\n',frameNr);
    
    % read the frame
    depthfile = sprintf(depthfileformat,frameNr);
    
    if(~exist(depthfile, 'file'))
        continue;
    end
    
    depth = depthRead(depthfile);
    depth = 1./depth; %make inverse depth as depth
    depth(depth > farF) = 0.0;
    
    XYZcam = depth2XYZcamera(K, depth);
    image = imread(sprintf(imgfileformat,frameNr));
    
    
    if(frameNr == startnum)
        origdepth = depth;
        origdepth(origdepth > farF) = 0.0;
        origdepth = imadjust(mat2gray(origdepth,[nearF 500]));
        GrayIndex34 = uint8(floor(origdepth * 255));
        Map       = jet(255);
        RGB34      = ind2rgb(GrayIndex34, Map);
        origimage = image;
    end
    
    Ncam = vertex2normal(XYZcam);
    camRtC2W = cameraRtC2W(:,:,k);
    
    %% update TSDF
    integrate(camRtC2W);
    
    %% ray casting for the result
    % ray casting result is in the world coordinate
    [VMap,~,~,~] = raycastM([eye(3) [0;0;0]], [nearF farF]);
    %[VMap,NMap,tMap] = raycastingTSDFdump(camRtC2WrayCasting, [0.4 8]);
    
    newDepth = reshape(VMap(3,:),height,width);
    
    
    VMap = reshape(double(VMap'),height,width,3);
    VMap(:,:,4) = ~isnan(VMap(:,:,1));
    NMap = vertex2normal(VMap);
    % normalize normal map
    
    
    subplot(2,3,4);
    imshow(RGB34);
    title('Input Depth','FontSize', 7);
    
    
    subplot(2,3,1)
    imshow(origimage);
    title('Input Image','FontSize', 7);
    
    
    subplot(2,3,2);
    imshow(image);
    title('Current Image','FontSize', 7);
    
    subplot(2,3,5);
    depth(depth > farF) = 0.0;
    depth = imadjust(mat2gray(depth,[nearF 500]));
    GrayIndex35 = uint8(floor(depth * 255));
    RGB35      = ind2rgb(GrayIndex35, Map);
    imshow(RGB35);
    title('Current Depth','FontSize', 7);
    
    subplot(2,3,3)
    imshow(newDepth);
    newDepth(newDepth > farF) = 0.0;
    newDepth = imadjust(mat2gray(newDepth,[nearF 500]));
    GrayIndex33 = uint8(floor(newDepth * 255));
    RGB33      = ind2rgb(GrayIndex33, Map);
    imshow(RGB33);
    title('Fused Depth','FontSize', 7);
    
    subplot(2,3,6)
    raycastingDirectionW = transformDirection(raycastingDirectionC,[eye(3) zeros(3,1)]);
    DotMap = reshape(max(0,sum(-reshape(NMap,[height*width 3])' .* raycastingDirectionW,1)),[height width]);
    imagesc(DotMap);
    colormap('gray'); axis equal; axis tight; axis off
    title('Fused Phong','FontSize', 7);
    
    drawnow;
    saveas(h,sprintf(saveimageformat, max_variance, unit, skip,startnum,k));
    
    k = k+1;    
    if(frameNr == endnum)
        ff = strcat(saveimageformat, '.txt');
        dlmwrite(sprintf(ff,max_variance,unit, skip,startnum,k),newDepth);
    end
end


end



