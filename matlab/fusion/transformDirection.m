function newDirection = transformDirection(Direction,Rt)
% Transform a direction into a new direction

newDirection = Rt(1:3,1:3) * Direction;

