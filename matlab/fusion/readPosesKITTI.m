function [ cameraRtC2W, camIndices ] = readPosesWhalley( priorfileformat, posefileformat, startnum, endnum, skip, usePrior)

import gtsam.*;

% number of frames
nrFrames = floor((endnum-startnum)/skip + 1);
cameraRtC2W = repmat([eye(3) zeros(3,1)], [1,1,nrFrames]);

prevpose = gtsam.Pose3();

% prevpose = cam2ned.compose(prevpose);
% gtsam.plotPose3(prevpose,[],scale);

skipcounter = 1;
counter = 0;
camIndices = [];
for frame = startnum : 1 : endnum        
    vofilename = sprintf(posefileformat, frame);
    priorfilename = sprintf(priorfileformat, frame);
    if(~exist(vofilename, 'file'))
        continue;
    end
    if(nargout == 2)
        camIndices = [camIndices; frame];   
    end
    
    prior = dlmread(priorfilename);
    vo = dlmread(vofilename);
    if(usePrior)
        deltapose = gtsam.Pose3.Expmap([-prior(1,3) -prior(1,1) prior(1,2) -prior(2,3) -prior(2,1) prior(2,2)]');
    else
        baseline = norm(prior(2,:),2);
        deltapose = gtsam.Pose3.Expmap([-vo(1,3) -vo(1,1) vo(1,2) -baseline*vo(2,3) -baseline*vo(2,1) baseline*vo(2,2)]');
    end
    
    curpose = prevpose.compose(deltapose);
    prevpose = curpose;
    counter = counter + 1;
    
    if(mod(counter,skip) == 0)
        X = prevpose.matrix();
        cameraRtC2W(:,:,skipcounter) = X(1:3,:);
        skipcounter = skipcounter+1;
        counter =0;
    end
end

end

