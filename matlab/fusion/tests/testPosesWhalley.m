% test the camera poses
import gtsam.*;
addpath('../');
posefileformat = '/home/nsrinivasan7/Dropbox/Whalley_sim_noSky_11-29-2016/cam/vo/vo_%08d.txt';
priorfileformat = '/home/nsrinivasan7/Dropbox/Whalley_sim_noSky_11-29-2016/cam/prior/prior_%08d.txt';
rawimagefileformat = '/home/nsrinivasan7/Dropbox/Whalley_sim_noSky_11-29-2016/cam/raw/raw_%08d.png';
depthimageformat = '/home/nsrinivasan7/Dropbox/Whalley_sim_noSky_11-29-2016/cam/range_txt/range_txt_%08d.txt';

skip = 1;
startnum = 4327;
endnum = 6169;
usePrior = true;

hold on;

[cameraRtC2W_prior, camIndices] = readPosesWhalley( priorfileformat, posefileformat, ...
    startnum, endnum, skip, usePrior);

usePrior = false;
[cameraRtC2W_vo, camIndices] = readPosesWhalley( priorfileformat, posefileformat, ...
    startnum, endnum, skip, usePrior);

figure(1);
hold on;

xval = zeros(size(cameraRtC2W_prior,3),1);
yval = zeros(size(cameraRtC2W_prior,3),1);
zval = zeros(size(cameraRtC2W_prior,3),1);

for i = 1:size(cameraRtC2W_prior,3)  
 
    depthImage = sprintf(depthimageformat, camIndices(i))
    depth = depthRead(depthImage);
    depth = 1./depth; %make inverse depth as depth
    depth(depth > 2000.0) = 0.0;
    image(depth);
    colormap('jet');
    drawnow;
    keyboard;
    
end

hold off;