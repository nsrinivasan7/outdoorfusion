function XYZcamera = depth2XYZcamera(K, depth)
width = size(depth,2);
height = size(depth,1);
[x,y] = meshgrid(1:width, 1:height);
XYZcamera(:,:,1) = (x-K(1,3)).*depth/K(1,1);
XYZcamera(:,:,2) = (y-K(2,3)).*depth/K(2,2);
XYZcamera(:,:,3) = depth;
XYZcamera(:,:,4) = depth~=0;
end


