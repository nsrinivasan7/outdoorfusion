figure(1);
subplot(1,2,1);
h12 = animatedline;
axis([0,4*pi,-1,1])

subplot(1,2,2);
h22 = animatedline;
axis([0,4*pi,-1,1])

x = linspace(0,4*pi,1000);
y = sin(x);
for k = 1:length(x)
    addpoints(h12,x(k),y(k));
    addpoints(h22,x(k),y(k));
    drawnow
end