%script file to compare VO performances
clear all; close all;
% read in the VO output


%specify the VO(Nister) directory
vodir_nister = '//home/nsrinivasan7/borg/soar2/build/source/scripts/IR/cam/vo_nister/';
%specify the VO(FixedRotation) directory
vodir_fixedRotation = '//home/nsrinivasan7/borg/soar2/build/source/scripts/IR/cam/vo_fixedRotation/';
%specify the VO(Nonlinear) directory
vodir_nonlinear = '/home/nsrinivasan7/borg/soar2/build/source/scripts/IR/cam/vo_nonlinear/';


%specify the prior directory
priordir = '/home/nsrinivasan7/borg/soar2/build/source/scripts/IR/cam/prior/';
%specify the flow image directory
flowdir = '/home/nsrinivasan7/borg/soar2/build/source/scripts/IR/cam/flow/';
%specify the undostorted image directory
rawdir = '/home/nsrinivasan7/borg/soar2/build/source/scripts/IR/cam/raw/';
%specify the undostorted image directory
depthdir = '/home/nsrinivasan7/borg/soar2/build/source/scripts/IR/cam/range/';

%startfilenumber
startfilenumber = 17;
endfilenumber = 1092;
skip = 1;
numberofframes = (endfilenumber - startfilenumber)/skip + 1;

vofileformat_nister = [vodir_nister 'vo_%08d.txt'];
vofileformat_fixedRotation = [vodir_fixedRotation 'vo_%08d.txt'];
vofileformat_nonlinear = [vodir_nonlinear 'vo_%08d.txt'];

flowfileformat = [flowdir 'flow_%08d.png'];
rawfileformat = [rawdir, 'raw_%08d.png'];
depthfileformat = [depthdir 'range_%08d.png'];

priorfileformat = [priordir 'prior_%08d.txt'];

vos_nister = zeros(numberofframes, 6);
vos_fixedRotation = zeros(numberofframes, 6);
vos_nonlinear = zeros(numberofframes, 6);

priors = zeros(numberofframes,6);

k = 1;
for frame = startfilenumber : skip : endfilenumber
    voFile_nister = sprintf(vofileformat_nister, frame);
    voFile_fixedRotation = sprintf(vofileformat_fixedRotation, frame);
      
    
    priorFile = sprintf(priorfileformat, frame);
    
    %check if file exists
    
    if exist (voFile_nister, 'file') 
        vo_nister = dlmread(voFile_nister);
        vos_nister(k,:) = reshape(vo_nister',1,6);
    else
        vos_nister(k,:) = vos_nister(k-1,:);
    end
    
    if exist(priorFile, 'file') 
        prior = dlmread(priorFile);
        priors(k,:) = reshape(prior',1,6);
        priors(k,4:6) = priors(k,4:6)/norm(priors(k,4:6));
    else
        priors(k,:) = priors(k-1,:);
    end
    
    if exist(voFile_fixedRotation, 'file')
        vo_fixedRotation = dlmread(voFile_fixedRotation);
        vos_fixedRotation(k,:) = reshape(vo_fixedRotation',1,6);
    else
        vos_fixedRotation(k,:) = vos_fixedRotation(k-1,:);
    end
        
    k = k +1;
end

h = figure('units','normalized','outerposition',[0 0 1 1]);
%h = figure(1);
hold on;

subplot(3,3,1);
h231_vo_nister = animatedline('Color',[1 0 0]);
h231_vo_fixedRotation = animatedline('Color',[0.7 0.3 0]);
h231_vo_nonlinear = animatedline('Color',[0.5 0.5 0]);
h231_prior = animatedline('Color',[0 0 1]);
%axis([1,numberofframes, -8*10^-3, 6*10^-3]);
xlim([1, numberofframes]);
legend({'VO(Nister)', 'VO(fixedRotation)', 'VO(nonlinear)','IMU'},'Position',[0.01 0.6,0.1 0.1]);
title('roll');

subplot(3,3,2);
h232_vo_nister = animatedline('Color',[1 0 0]);
h232_vo_fixedRotation = animatedline('Color',[0.7 0.3 0]);
h232_vo_nonlinear = animatedline('Color',[0.5 0.5 0]);
h232_prior = animatedline('Color',[0 0 1]);
%axis([1,numberofframes, -8*10^-3, 4*10^-3]);
xlim([1, numberofframes]);
title('pitch');


subplot(3,3,3);
h233_vo_nister = animatedline('Color',[1 0 0]);
h233_vo_fixedRotation = animatedline('Color',[0.7 0.3 0]);
h233_vo_nonlinear = animatedline('Color',[0.5 0.5 0]);
h233_prior = animatedline('Color',[0 0 1]);
%axis([1,numberofframes, -8*10^-3,4*10^-3]);
xlim([1, numberofframes]);
title('yaw');


subplot(3,3,4);
h234_vo_nister = animatedline('Color',[1 0 0]);;
h234_vo_fixedRotation = animatedline('Color',[0.7 0.3 0]);
h234_vo_nonlinear = animatedline('Color',[0.5 0.5 0]);
h234_prior = animatedline('Color',[0 0 1]);
%axis([1,numberofframes,-0.2,1]);
xlim([1, numberofframes]);
title('\delta-directionx');


subplot(3,3,5);
h235_vo_nister = animatedline('Color',[1 0 0]);;
h235_vo_fixedRotation = animatedline('Color',[0.7 0.3 0]);
h235_vo_nonlinear = animatedline('Color',[0.5 0.5 0]);
h235_prior = animatedline('Color',[0 0 1]);
%axis([1,numberofframes, -0.1,1]);
xlim([1, numberofframes]);
title('\delta-directiony');


subplot(3,3,6);
h236_vo_nister = animatedline('Color',[1 0 0]);;
h236_vo_fixedRotation = animatedline('Color',[0.7 0.3 0]);
h236_vo_nonlinear = animatedline('Color',[0.5 0.5 0]);
h236_prior = animatedline('Color',[0 0 1]);
%axis([1,numberofframes, -0.025,0.005]);
xlim([1, numberofframes]);
title('\delta-directionz');

flow = [];
depth = [];
raw = [];

saveformat = 'result/IMUComparision_%08d';
if ~exist('result','dir')
    mkdir result;
end

for frame =  startfilenumber : skip : endfilenumber
    
    addpoints(h231_vo_nister,frame,vos_nister(frame,1));
    addpoints(h231_vo_fixedRotation,frame,vos_fixedRotation(frame,1));
    addpoints(h231_vo_nonlinear,frame,vos_nonlinear(frame,1));
    addpoints(h231_prior,frame,priors(frame,1) );
    
    
    addpoints(h232_vo_nister,frame,vos_nister(frame,2));
    addpoints(h232_vo_fixedRotation,frame,vos_fixedRotation(frame,2));
    addpoints(h232_vo_nonlinear,frame,vos_nonlinear(frame,2));
    addpoints(h232_prior,frame,priors(frame,2) );
        
    addpoints(h233_vo_nister,frame,vos_nister(frame,3));
    addpoints(h233_vo_fixedRotation,frame,vos_fixedRotation(frame,3));
    addpoints(h233_vo_nonlinear,frame,vos_nonlinear(frame,3));
    addpoints(h233_prior,frame,priors(frame,3) );
        
    addpoints(h234_vo_nister,frame,vos_nister(frame,4));
    addpoints(h234_vo_fixedRotation,frame,vos_fixedRotation(frame,4));
    addpoints(h234_vo_nonlinear,frame,vos_nonlinear(frame,4));
    addpoints(h234_prior,frame,priors(frame,4) );
        
    addpoints(h235_vo_nister,frame,vos_nister(frame,5));
    addpoints(h235_vo_fixedRotation,frame,vos_fixedRotation(frame,5));
    addpoints(h235_vo_nonlinear,frame,vos_nonlinear(frame,5));
    addpoints(h235_prior,frame,priors(frame,5) );
    
    addpoints(h236_vo_nister,frame,vos_nister(frame,6));
    addpoints(h236_vo_fixedRotation,frame,vos_fixedRotation(frame,6));
    addpoints(h236_vo_nonlinear,frame,vos_nonlinear(frame,6));
    addpoints(h236_prior,frame,priors(frame,6) );
    
    
    flowimagefile = sprintf(flowfileformat, frame);
    depthimagefile = sprintf(depthfileformat, frame);
    rawimagefile = sprintf(rawfileformat, frame);
    
    subplot(3,3,7);
    if exist(flowimagefile, 'file')
        flow = imread(flowimagefile);
    end
    imshow(flow);
    title('flow (nister)');
    
    subplot(3,3,8);
    if exist(depthimagefile, 'file')
        depth = imread(depthimagefile);
    end
    imshow(depth);
    title('depth (nister)');
    
    subplot(3,3,9);
    if exist(rawimagefile, 'file')
        raw = imread(rawimagefile);
    end
    imshow(raw);
    title('raw');
       
    savefile = sprintf(saveformat, frame);
    %saveas(h,savefile);
    img = getframe(gcf);
    imwrite(img.cdata, [savefile, '.png']);
    pause(0.1);

end